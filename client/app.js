var app = new Framework7({
    // App root element
    root: '#app',
    // App Name
    name: 'VIP Trading Apps Monitor',
    // App id
    id: 'org.nyandevid.viptradingmonitor'
});

var $$ = Dom7;

var socket = new io('ws://127.0.0.1:3001')
var baseGauge = {
    type: 'semicircle',
    value: 0.0,
    size: 250,
    borderColor: '#2196f3',
    borderWidth: 10,
    valueText: '0%',
    valueFontSize: 41,
    valueTextColor: '#2196f3',
}

var cpuCircle = app.gauge.create(Object.assign({ el: '.gauge.cpu', labelText: "CPU of consumption" }, baseGauge))
var sdramCircle = app.gauge.create(Object.assign({ el: '.gauge.sdram', labelText: "SDRAM of consumption" }, baseGauge))
var sessionCircle = app.gauge.create(Object.assign({ el: '.gauge.session', labelText: "Session of used" }, baseGauge))

var mock = {
    "req": {
      "ip": "192.168.0.56"
    },
    "data": {
      "_v": true,
      "ports": [
        {
          "type": "wlan",
          "port": 5,
          "status": true
        },
        {
          "type": "lan",
          "port": 4,
          "status": false
        },
        {
          "type": "lan",
          "port": 3,
          "status": false
        },
        {
          "type": "lan",
          "port": 2,
          "status": false
        },
        {
          "type": "lan",
          "port": 1,
          "status": true
        }
      ],
      "wifi": {
        "connectedClients": 2,
        "mode": "Local AP",
        "band": "2.4 GHz (B+G+N)",
        "ssid": "STIKES ICME - IT",
        "channelNumber": 6,
        "encryption": "WPA2(AP),Disabled(WDS)",
        "bssid": "78:44:76:e8:8c:ac",
        "wpsStatus": "Off"
      },
      "lan": {
        "ip": {
          "address": "192.168.0.56",
          "subnetMask": "255.255.255.0",
          "gateway": "192.168.0.1"
        },
        "connectionType": "DHCP",
        "dhcpServer": "",
        "macAddress": "78:44:76:e8:8c:ac",
        "connectedClients": 1
      },
      "system": {
        "device": {
          "cpu": 1,
          "sdram": 23
        },
        "uptime": "2day:20h:57m:25s",
        "firmwareVersion": "TOTOLINK-N300RT-V2.2.0-B20170525.1552",
        "buildTime": "Mon Dec 12 18:01:09 CST 2016",
        "maxSessions": 20000,
        "currentSessions": 5
      }
    }
  }

angular.module('webapps', [])
.controller('cards', function($scope, $http) {
    socket.emit('getDevices')

    $scope.devices = []
    $scope.device = null
    $scope.loading = false

    $scope.isSelectedNull = function() {
        return ($scope.device === null)
    }
    $scope.getActivePorts = function(ports) {
        return Array.from(ports.filter(port => port.status)).length
    }
    $scope.intitalizeGauge = function(device) {
        // return { decimal, text }
        var values = {
            cpu: {
                decimal: device.data.system.device.cpu / 100,
                text: device.data.system.device.cpu + ' %'
            },
            sdram: {
                decimal: device.data.system.device.sdram / 100,
                text: device.data.system.device.sdram + ' %'
            },
            session: {
                decimal: device.data.system.currentSessions / device.data.system.maxSessions,
                text: (device.data.system.currentSessions / device.data.system.maxSessions * 100).toFixed(2) + ' %'
            }
        } 

        cpuCircle.update({
            value: values.cpu.decimal,
            valueText: values.cpu.text
        })
        sdramCircle.update({
            value: values.sdram.decimal,
            valueText: values.sdram.text
        })
        sessionCircle.update({
            value: values.session.decimal,
            valueText: values.session.text
        })
    }

    $scope.getDetail = function(device) {
        console.log('get Detail', device.ip)

        $scope.device = null
        $scope.loading = true

        socket.emit('get', { ip: device.ip })
    }

    socket.on('devices', function(devices) {
        console.log(devices)

        $scope.$apply(function() {
            $scope.devices = devices
        })
    })
    socket.on('get', function(device) {
        console.log(device)

        $scope.$apply(function() {
            $scope.device = device
            $scope.loading = false

            $scope.intitalizeGauge(device)
        })
    })

    if ($scope.device !== null) {
        $scope.intitalizeGauge($scope.device)
    }
})