const parser = require('../services/parser')
const tools = require('../services/tools')

async function main() {
    try {
        const availableTotolink = await tools.scan('192.168.2.0/24', 1000)
        const status = await parser.getStatus(Array.from(availableTotolink).map(item => item.address), {})

        console.log(status)
    } catch(error) {
        console.error(error)
    }
}

main()