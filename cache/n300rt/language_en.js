
var reboot_confirm="Do you really want to reboot this router?";
var page_title='Wireless 11n Router';
var save_changes='Apply';
var connect_changes='Connect';
var disconnect_changes='Disconnect';
var ghz_ghz='GHz';
var refresh_refresh = ' Refresh ';


/*************************tunnel6.htm*****************************/
var tunnel6_header="Configuring Tunnel(6 to 4)";
var tunnel6_enable="Enable";
var tunnel6_save="Save";


/****************************dnsv6.htm**********************/
var dnsv6_header="Configuring DNSv6";
var dnsv6_enable="Enable";
var dnsv6_routename="Router Name";
var dnsv6_save="Save";

/*********************radvd.htm********************************/
var radvd_header="Configuring Router Advertisement";
var radvd_enable="Enable";
var radvd_interfacename="Radvd Interface Name";
var radvd_maxrtxadvinterval="Max Rtr Adv Interval";
var radvd_minrtxadvinterval="Min Rtr Adv Interval";
var radvd_mindelay="Min Delay Between RAs";
var radvd_advmanagedflag="Adv Managed Flag";
var radvd_advotherconfigflg="Adv Other Config Flag";
var radvd_linkmtu="Adv Link MTU";
var radvd_reachabletime="Adv Reachable Time";
var radvd_retrandtimer="Adv Retrans Timer";
var radvd_curhoplimit="Adv Cur Hop Limit";
var radvd_defaultlifetime="Adv Default Lifetime";
var radvd_defaultpreference="Adv Default Preference";
var radvd_sourcelladdress="Adv Source LL Address";
var radvd_unicastonly="Unicast Only";
var radvd_enabled_0="Enabled Prefix1";
var radvd_AdvOnLinkFlag_0="Adv On Link Flag";
var radvd_AdvAutonomousFlag_0="Adv Autonomous Flag";
var radvd_AdvValidLifetime_0="Adv Valid Lifetime";
var radvd_AdvPreferredLifetime_0="Adv Preferred Lifetime";
var radvd_AdvRouterAddr_0="Adv Router Addr";
var radvd_if6to4_0="If 6 to 4";
var radvd_Enabled_1="Enabled Prefix2";
var radvd_submit_save="Save";



/*********************ipv6_basic.htm************/
var ipv6_basic_header="IPv6 Addr Setting";
var ipv6_basic_wan_address="WAN Address";
var ipv6_basic_lan_address="LAN Address";
var ipv6_basic_save="Save";

/**********************ipv6_wan.htm****************/
var ipv6_enable='Enable IPv6';
var ipv6_dhcpv4_disable='DHCPv4 Disable';
var ipv6_origin_type='Origin Type:';
var ipv6_prefix_length='Prefix Length';
var ipv6_dhcpmode_stateless='Stateless Address Auto Configuration';
var ipv6_dhcpmode_stateful='Stateful Address Auto Configuration';
var ipv6_pd_enable='PD Enable:';
var ipv6_rapid_comment='Rapid-commit Enable:';
var ipv6_dns_setting='DNS Setting';
var ipv6_dns_config='Configuring DNSv6';
var ipv6_other='Others';
var ipv6_mld_proxy='Enable MLD Proxy';
var ipv6_pppoe_mtu_invalid='Invalid PPPoE MTU size!';
var ipv6_address_invalid=' Invalid IPv6 address!';
var ipv6_address='IPv6 Address';
var ipv6_prefix_value_msg='IPv6 prefix must between 0-128!';
var ipv6_mac_address_msg1='MAC address should not be broadcast MAC ffffffffffff';
var ipv6_mac_address_msg2='MAC address should not be multicast MAC address between 01005e000000 and 01005e7fffff.';
var ipv6_mac_address_msg3='Invalid MAC address. It should not be 00:00:00:00:00:00.';
var ipv6_mac_address_msg4='Invalid MAC address. It should not be ff:ff:ff:ff:ff:ff.';
var ipv6_mac_address_msg5='Invalid MAC address. It should not be multicast MAC address between 01:00:5e:00:00:00 and 01:00:5e:7f:ff:ff.';
var ipv6_dns_address_invalid=' Invalid DNS address!';
var ipv6_start_address_invalid='Invalid start address!';
var ipv6_end_address_invalid='Invalid end address!';
var ipv6_lan_address_invalid='Invalid LAN address!';
var ipv6_first_prefix_msg='Invalid first prefix address!';
var ipv6_second_prefix_msg='Invalid second prefix address!';
var ipv6_local_ip_addr='Local IPv6 Address';
var ipv4_local_ip_addr='Local IPv4 Address';
var ipv4_enable='Enable IPv4';
var menu_ipv6_wan='IPv6  WAN  Setting';
var menu_ipv6_lan='IPv6  LAN  Setting';

/*********************************dhcp6s.htm*****************/
var dhcp6s_header = "Configuring LAN IP Address and DHCPv6 Options";
var dhcp6s_enabled = "Enable IPv6 DHCP ";
var dhcp6s_addrpoolstart = "DNS Addr Pool From";
var dhcp6s_addrpoolend = "To";
var dhcp6s_dnsaddr = "DNS Address";
var dhcp6s_interfacenameds = "Interface Name";
var dhcp6s_addrspool = "DHCP Addrs Pool";
var dhcp6s_save = "Save";

var dhcp6s_ULA = "Configuring ULA setting";
var dhcp6s_enb = "Enable ULA";
var dhcp6s_IPAddr = "IP Address:";
var dhcp6s_AddrPre = "Addrs Prefix:";
var dhcp6s_Manu = "Manually";
var dhcp6s_PreDel = "Prefix Delegation";
var dhcp6s_ULAPre = "ULA Prefix";

/************.c*****************************/
var L2TP_Disconnected = "L2TP Disconnected";
var L2TP_Connected="L2TP Connected";
var PPTP_Disconnected = "PPTP Disconnected";
var PPTP_Connected="PPTP Connected";
var PPPoE_Disconnected = "PPPoE Disconnected";
var PPPoE_Disconnected_failed = "PPPoE Disconnectedï¼šAuthentication Failed";
var PPPoE_Disconnected_not_find = "PPPoE Disconnectedï¼šNot Found PPPoE Server";
var PPPoE_Disconnected_not_connect = "PPPoE Disconnectedï¼šWAN Not Connected";
var PPPoE_Disconnected_no_response = "No response from the Server side";
var PPPoE_Connected="PPPoE Connected";
var Fixed_IP_Disconnected = "Static IP Disconnected";
var Fixed_IP_Connected="Static IP Connected";
var Getting_IP_from_DHCP_server ="Getting IP from DHCP Server...";
var USB3G_Connected = "USB3G Connected";
var USB3G_Modem_Initializing = "USB3G Modem Initializing...";
var USB3G_Dialing = "USB3G Dialing...";
var USB3G_Removed = "USB3G Removed";
var USB3G_Disconnected = "USB3G Disconnected";
var Brian_5BGG = "Brian 5BGG";
var DHCP = "DHCP";
var Static_IP = "Static IP";
var Dynamic_IP = "Dynamic IP";
var free_string = "";
var Secondary_Wan_Connected = "Secondary WAN Connected";
var Secondary_Wan_Disconnected = "Secondary WAN Disconnected!";
var Local_AP = "Local AP";
var ap = "(AP),";
var day = "day";
var hour = "h";
var min = "m";
var sec = "s";
var Fixed_IP = "Static IP";
var quicksetup_save = "Apply";
var quicksetup_enabled = "IPTV";
var quicksetup_iptv1_port = "IPTV PORT";
var quicksetup_iptv2_port = "VoIP PORT";
var dhcpleasetime = "(1 ~ 10080 minutes)";
var idletime = "(1-1000)";
var default_web_server_port = "(default 80)";
var default_ssh_server_port = "(default 22)";
var default_wireless_point = "(default 64)";
var rebootschedule_hour = "(0-23 hour)";
var rebootschedule_min = "(0-59 min)";
var Disabled="Disabled";
var WEP_64bits="WEP 64bits";
var WEP_128bits="WEP 128bits";
var wps_status_off='Off';
var wps_status_on='On';
var dns1="DNS 1";
var dns2="DNS 2";
var dns3="DNS 3";
var dns4="Internet DNS 1";
var dns5="Internet DNS 2";
var dns6="Internet DNS 3";
var cpuinfo="CPU Load";
var meminfo="SDRAM Utilization";




/******************snmp.htm****************/
var snmp_header='SNMP Setting';
var snmp_explain=' ';
var snmp_enabled='Enable SNMP ';
var snmp_name='Name ';
var snmp_location='Location ';
var snmp_contact='Contact ';
var snmp_read_write_conmmunity='Read/Write Community ';
var snmp_read_only_conmmunity='Read-Only Community ';
var snmp_name_empty="SNMP Name can't be empty";
var snmp_location_empty="SNMP Location can't be empty";
var snmp_contanct_empty="SNMP Contact can't be empty";
var snmp_read_write_conmmunity_empty="Read/Write Community can't be empty";
var snmp_read_only_conmmunity_empty="Read only Community can't be empty";



/************tr069config.htm*****************/
var tr069_header='TR-069 config';
var tr069_explain=' On this page you can configure the TR-069 CPE. Here you can change the setting of ACS.';
var tr069_button='TR-069';
var tr069_off='Off';
var tr069_on='On';
var tr069_acs='ACS';
var tr069_url='URL';
var tr069_user_name='User Name';
var tr069_password='Password';
var tr069_enable_periodic='Enable Periodic Information';
var tr069_interval_periodic='Interval Periodic Updates';
var tr069_query='Connection Request';
var tr069_path='Path';
var tr069_port='Port';
var tr069_remind1='ACS URL can not be empty!';
var tr069_remind2='Invalid User Name!';
var tr069_remind3='Invalid Password!';
var tr069_remin4='Please enter a predetermined time interval.';
var tr069_remind5='The interval should be written in decimals (0-9).';
var tr069_remind6='Invalid Password!';
var tr069_remind7='Invalid Password!';
var tr069_remind8='Wrong Direction!';
var tr069_remind9='Please enter the port number for connection request';
var tr069_remind10='Invalid port number for connection request. Please type the port number ranging from 1 to 65535.';
var tr069_remind11='Invalid port number for connection request. Please type the port number ranging from 1 to 65535.';
/**************************tr069config.htm********************/



/*****************syscmd.htm************************/
var syscmd_header = 'System Command';
var syscmd_explain = 'This page can be used to run target system command.';
var syscmd_command = 'System Command';
var syscmd_apply = 'Apply';
var syscmd_refresh = 'Refresh';
var syscmd_alert1 = 'Please add "-c num" to ping command';
var syscmd_alert2 = 'Command can not be empty';


/***********	wlstatbl.htm	************/
var wlstatbl_tbl_name = 'Active Wireless Client Table';
var wlstatbl_explain = ' This table shows the MAC address, transmission, receiption packet counters and encrypted status for each associated wireless client.';
var wlstatbl_mac = 'MAC Address';
var wlstatbl_mode = 'Mode';
var wlstatbl_tx = 'Tx Packet';
var wlstatbl_rx = 'Rx Packet';
var wlstatbl_tx_rate ='Tx Rate (Mbps)';
var wlstatbl_ps = 'Power Saving';
var wlstatbl_expired_time = 'Expired Time (s)';
var wlstatbl_refresh = 'Refresh';
var wlstatbl_close = 'Close';
var wlstatbl_vap_wrong = 'Unable to process the AP.';


/***********	ip_qos.htm	************/
var ip_qos_header = 'QoS';
var ip_qos_header_explain = ' Entries in this table improve your online gaming experience by ensuring that your game traffic is prioritized over other network traffic, such as FTP or Web.';
var ip_qos_enable = 'QoS';
var ip_qos_auto_upspeed = 'Automatic Uplink Speed';
var ip_qos_manu_upspeed = 'Manual Uplink Speed (Kbps)';
var ip_qos_auto_downspeed = 'Automatic Downlink Speed';
var ip_qos_manu_downspeed = 'Manual Downlink Speed (Kbps)';
var ip_qos_rule_set = 'QoS Rule Setting';
var ip_qos_addrtype = 'Address Type';
var ip_qos_addrtype_ip = 'IP';
var ip_qos_addrtype_mac = 'MAC';
var ip_qos_local_ipaddr = 'Local IP Address';
var ip_qos_proto = 'Protocol';
var ip_qos_proto_udp = 'UDP';
var ip_qos_proto_tcp = 'TCP';
var ip_qos_proto_both = 'Both';
var ip_qos_local_port = 'Local Port(1~65535)';
var ip_qos_macaddr = 'MAC Address';
var ip_qos_mode = 'Mode';

var ip_qos_upband = 'Uplink Bandwidth (Kbps)';
var ip_qos_downband = 'Downlink Bandwidth (Kbps)';
var ip_qos_apply = 'Apply';
var ip_qos_reset = 'Reset';
var ip_qos_curr_qos = 'Current QoS Rules Table';
var ip_qos_delete_select_btn = 'Delete Selected';
var ip_qos_delete_all_btn = 'Delete All';

var ip_qos_upspeed_empty = 'Manual Uplink Speed cannot be empty or less than 100 when Automatic Uplink Speed is disabled.';
var ip_qos_downspeed_empty = 'Manual Downlink Speed cannot be empty or less than 100 when Automatic Downlink Speed is disabled.';
var ip_qos_ip_invalid = 'Invalid IP address';
var ip_qos_startip_invalid = 'Invalid start IP address! It should be set within the current Subnet.';
var ip_qos_portrange_invalid = 'Invalid port range! It should be 1~65535.';
var ip_qos_macaddr_notcomplete = 'Input MAC address is not complete. It should be 12 digits in hex.';
var ip_qos_macaddr_invalid = 'Invalid MAC address. It should be in hex number (0-9 or a-f).';
var ip_qos_band_empty = 'Uplink Bandwidth or Downlink Bandwidth cannot be empty.';
var ip_qos_band_invalid = 'Invalid input! It should be decimal number (0-9).';
var ip_qos_delete_select = 'Do you really want to delete the selected entry?';
var ip_qos_delete_all = 'Do you really want to delete all entries?';

var ip_qos_tbl_localaddr = 'Local IP Address';
var ip_qos_tbl_macaddr = 'MAC Address';
var ip_qos_tbl_mode = 'Mode';
var ip_qos_tbl_upband = 'Uplink Bandwidth';
var ip_qos_tbl_downband = 'Downlink Bandwidth';
var ip_qos_tbl_select = 'Select';
var ip_qos_restrict_maxband = "Restricted Maximum Bandwidth";
var ip_qos_quarant_minband = "Guaranteed Minimum Bandwidth";


/***********	vlan.htm ************/
var iptv_header = 'IPTV Settings';
var vlan_header = 'IPTV Settings';
var vlan_header_explain = 'Entries in below table are used to config vlan settings.VLANs are created to provide the segmentation services traditionally provided by routers. VLANs address issues such as scalability, security, and network management.';
var vlan_apply = 'Apply';
var vlan_reset = 'Reset';

var vlan_enable = 'Enabled';
var vlan_disable = 'Disabled';
var vlan_triple_play = 'Triple Play / IPTV';
var vlan_id = 'VLAN ID';
var vlan_tagtbl = 'Tag Table';
var vlan_tagtbl_interface = 'Interface Name';
var vlan_tagtbl_taged = 'Taged';
var vlan_tagtbl_untaged = 'Untaged';
var vlan_tagtbl_notin = 'Not in this VLAN';

var vlan_settbl = 'Current VLAN Setting Table';
var vlan_settbl_id = 'VLAN ID';
var vlan_settbl_taged = 'Taged Interface';
var vlan_settbl_untaged = 'Untaged Interface';
var vlan_settbl_modify = 'Modify';
var vlan_settbl_select = 'Select';
var vlan_deletechick = 'Delete Selected';
var vlan_deleteall = 'Delete All';

var vlan_nettbl = 'Current net interface setting table';
var vlan_nettbl_name = 'Interface Name';
var vlan_nettbl_pvid = 'PVID';
var vlan_nettbl_defprio = 'Default Priority';
var vlan_nettbl_defcfi = 'Default CFI';

var vlan_checkadd1 = 'Invalid VLAN ID. Must between 1-4094';
var vlan_checkadd2 = 'At least one interface should bind to this VLAN!!';
var vlan_deletesel = 'Do you really want to delete the selected entry?';
var vlan_deleteall_conf = 'Do you really want to delete all entries?';
var vlan_tbl_enable ='Enable';
var vlan_tbl_eth_wire ='Ethernet/Wireless';
var vlan_tbl_forwarding_rule='Forwarding Rule';
var vlan_tbl_wan_lan = 'WAN/LAN';
var vlan_tbl_tag = 'Tag';
var vlan_tbl_priority = 'Priority';
var vlan_tbl_cfi = 'CFI';
var vlan_tbl_vid ='VID';
var vlan_tbl_information = 'VLAN Information';
var vlan_tbl_port_map ='Port Mapping (LAN&nbsp;&amp;&nbsp;  WLAN)<br>&nbsp;P1&nbsp;P2&nbsp;P3&nbsp;P4&nbsp;W1&nbsp;W2&nbsp;W3&nbsp;';


/****************msg.htm**************************/
var save_set_ok = 'Save Setting Successfully!';
var reboot_router = 'Your changes have been saved. The router must be rebooted before new settings will take effect.';
var reboot_later = 'You can continue to make other changes and reboot later...';
var back_button = '<< Back';


/*******************countdownpage.htm*****************************/
var change_set_ok = 'Change Settings Successfully!';
var not_turn_off = 'Do not turn off or reboot the device during this time.';
var please_wait = 'Please Wait ';
var second = 'Seconds ...';


/***********	password.htm	************/
var password_header = 'Password Settings';
var password_header_explain = ' This page is used to set the account to access the web server of Access Point. Empty user name and password will disable the protection.';
var password_user_cname = 'Current User Name';
var password_user_cpasswd = 'Current Password';
var password_user_name = 'New User Name';
var password_user_passwd = 'New Password';
var password_user_passwd_confm = 'Confirmed New Password';
var password_user_passwd_confms = 'Confirmed Password';
var password_user_passwd_display='Unhide';
var password_passwd_unmatcheds = 'Password is not matched. Please type the same password.';
var password_apply = 'Apply';
var password_user_empty = 'User account is empty.\nDo you want to disable the password protection?';
var password_passwd_unmatched = 'Password is not matched. Please type the same password between \'new\' and \'confirmed\' box.';
var password_passwd_empty = 'Password cannot be empty. Please try again.';
var password_user_invalid = 'Cannot accept space character in User Name. Please try again.';
var password_passwd_invalid = 'Cannot accept space character in Password. Please try again.';
var password_reset = '  Reset  ';
var password_warning ='Forget Password?';
var password_warning_info ='Please press the RST button for about 10 seconds to make the device reset to default settings.\nThen refer to the device case to find default User Name and Password.';


/***********	saveconf.htm	************/
var saveconf_header = 'Save/Reload Settings';
var saveconf_header_explain = ' This page allows you save current settings to a file or reload the settings from the file which was saved previously. Besides, you could reset the current configuration to factory default.';
var saveconf_save_to_file = 'Save Settings to File';
var saveconf_save = 'Save...';
var saveconf_load_from_file = 'Load Settings from File';
var saveconf_load = 'Upload';
var saveconf_reset_to_default = 'Reset Settings to Default';
var saveconf_reset = 'Reset';
var saveconf_load_from_file_empty = 'Please select a file!';
var saveconf_reset_to_default_confm = 'Do you really want to reset the current settings to default?';


/***********	upload.htm	************/
var upload_header = 'Upgrade Firmware';
var upload_header_explain = ' This page allows you upgrade the Access Point firmware to new version. Please note, do not power off the device during the upload because it may crash the system.';
var upload_version = 'Firmware Version';
var upload_file_select = 'Select File';
var upload_send = 'Upgrade';
var upload_reset = 'Reset';
var upload_up_failed = 'Update firmware failed!';
var upload_send_file_empty = 'File name can not be empty !';
var upload_check_rfw_info = 'Select File';
var upload_check_rfw = 'Browse...';
var upgrade_mode = 'Upgrade mode'
var select_local_upgrade = 'Local --- Firmware from local PC'
var select_remote_upgrade = 'Network --- Firmware from network server'
var upload_downloading = 'Downloading  '
var upload_percentchar = ' %'

/***********	firewall	************/
var firewall_proto = 'Protocol';
var firewall_proto_both = 'Both';
var firewall_proto_tcp = 'TCP';
var firewall_proto_udp = 'UDP';
var firewall_add_rule = 'Add Rule';
var firewall_apply = 'Apply';
var firewall_reset = 'Reset';
var firewall_filtertbl = 'Current Filter Table';
var firewall_delete_select = 'Delete Selected';
var firewall_delete_all = 'Delete All';

var firewall_delete_selectconfm = 'Do you really want to delete the selected entry?';
var firewall_delete_allconfm = 'Do you really want to delete all entries?';
var firewall_ipaddr_invalid = 'Invalid IP address';
var firewall_port_notdecimal = 'Invalid port number! It should be decimal number (0-9).';
var firewall_port_toobig = 'Invalid port number! You should set a value between 1-65535.';
var firewall_port_rangeinvalid = 'Invalid port range! First port value should be less than second value.';
var firewall_port_wrong = 'Port number can not contain Web port';




var firewall_local_ipaddr = 'Local IP Address';
var firewall_port_range = 'Port Range ';
var firewall_port_internal = 'Internal Port';
var firewall_port_external ='External Port';
var firewall_remote_ipaddr ='Remote IP Address';
var firewall_comm = 'Comment';

var firewall_tbl_proto = 'Protocol';
var firewall_tbl_comm = 'Comment';
var firewall_tbl_select = 'Select';
var firewall_tbl_localipaddr = 'Local IP Address';
var firewall_portrange = 'Port Range';

/***********	portfilter.htm	************/
var portfilter_header = 'Port Filtering';
var portfilter_header_explain = ' Entries in this table are used to restrict certain types of data packets from your local network to Internet through the Gateway.  Use of such filters can be helpful in securing or restricting your local network.';
var portfiletr_enable = 'Port Filtering';
var portfilter_noport = 'You should set a port range for adding in an entry.';


/***********	ipfilter.htm	************/
var ipfilter_header = 'IP/Port Filtering';
var ipfilter_header_explain = ' Entries in this table are used to restrict certain types of data packets from your local network to Internet through the Gateway.  Use of such filters can be helpful in securing or restricting your local network.';
var ipfilter_header_explain1 = '';
var ipfilter_header_explain2 = '';
var ipfilter_enable = 'IP/Port Filtering';

/***********	Macfilter.htm	************/
var macfilter_header = 'MAC Filtering';
var macfilter_header_explain =  'Entries in this table are used to restrict certain types of data packets from your local network to Internet through the Gateway.  Use of such filters can be helpful in securing or restricting your local network.';
var macfilter_enable = 'MAC Filtering';
var macfilter_macaddr = 'MAC Address';
var macfilter_macaddr_nocomplete = 'Input MAC address is not complete. It should be 12 digits in hex.';
var macfilter_macaddr_nohex = 'Invalid MAC address. It should be in hex number (0-9 or a-f).';
var macfilter_filterlist_macaddr = 'MAC Address';

/***********	Portfw.htm	************/
var portfw_header = 'Port Forwarding';
var portfw_header_explain = 'Entries in this table allow you to automatically redirect common network services to a specific machine behind the NAT firewall.  These settings are only necessary if you wish to host some sort of server like a web server or mail server on the private local network behind your Gateway\'s NAT firewall.';//need xiugai
var portfw_header_explain1 = '';
var portfw_header_explain2 = '';
var portfw_enable = 'Port Forwarding';
var portfw_ipaddr = 'IP Address';
var portfw_apply_port_empty = 'Port range cannot be empty! You should set a value between 1-65535.';
var portfw_tbl = 'Current Port Forwarding Table';

/***********	urlfilter.htm	************/
var urlfilter_header = 'URL Filtering';
var urlfilter_header_explain = ' URL filter is used to deny LAN users from accessing the internet. Block those URLs which contain keywords listed below.';
var urlfilter_enable = 'URL Filtering';
var urlfilter_urladdr = 'URL Address';
var urlfilter_apply_error = 'Error character \";\"';
var urlfilter_filterlist_yrladdr = 'URL Address';

/***********	dmz.htm	************/
var dmz_header = 'DMZ';
var dmz_header_explain = 'A Demilitarized Zone is used to provide Internet services without sacrificing unauthorized access to its local private network. Typically, the DMZ host contains devices accessible to Internet traffic, such as Web (HTTP ) servers, FTP servers, SMTP (e-mail) servers and DNS servers.';
var dmz_header_exlain1 = '';
var dmz_header_exlain2='';
var dmz_enable = 'DMZ';
var dmz_host_ipaddr = 'DMZ Host IP Address';


/***********	dos.htm	************/
var dos_header = 'Denial of Service';
var dos_header_explain = 'A "denial-of-service" (DoS) attack is characterized by an explicit attempt by hackers to prevent legitimate users of a service from using that service.';
var dos_enable = 'Enable DoS Prevention';
var dos_packet_sec = ' Packets/Second';
var dos_sysflood_syn = 'Whole System Flood SYN';
var dos_sysflood_fin = 'Whole System Flood FIN';
var dos_sysflood_udp = 'Whole System Flood UDP';
var dos_sysflood_icmp = 'Whole System Flood ICMP';
var dos_ipflood_syn = 'Per-Source IP Flood SYN';
var dos_ipflood_fin = 'Per-Source IP Flood FIN';
var dos_ipflood_udp = 'Per-Source IP Flood UDP';
var dos_ipflood_icmp = 'Per-Source IP Flood ICMP';
var dos_portscan = 'TCP/UDP Port Scan';
var dos_portscan_low = 'Low';
var dos_portscan_high = 'High';
var dos_portscan_sensitivity = 'Sensitivity';
var dos_icmp_smurf = 'ICMP Smurf';
var dos_ip_land = 'IP Land';
var dos_ip_spoof = 'IP Spoof';
var dos_ip_teardrop = 'IP Tear Drop';
var dos_pingofdeath = 'Ping of Death';
var dos_tcp_scan = 'TCP Scan';
var dos_tcp_synwithdata = 'TCP Syn With Data';
var dos_udp_bomb = 'UDP Bomb';
var dos_udp_echochargen = 'UDP Echo Chargen';
var dos_select_all = ' Select ALL ';
var dos_clear_all = 'Clear ALL';
var dos_enable_srcipblocking = 'Enable Source IP Blocking';
var dos_block_time = 'Block Time (sec)';
var dos_apply = 'Apply';



/***********	route.htm ************/
var route_header = 'Routing Setup';
var route_header_explain = 'This page is used to setup dynamic routing protocol or edit static route entry.';
var route_enable = 'Enabled';
var route_disable = 'Disabled';
var route_apply = 'Apply';
var route_reset = 'Reset';

var route_dynamic = 'Enable Dynamic Route';
var route_nat = 'NAT';
var route_nat_enable = 'Enabled';
var route_nat_disable = 'Disabled';
var route_transmit = 'Transmit';
var route_receive = 'Receive';
var route_rip = 'Disabled';
var route_rip1 = 'RIP 1';
var route_rip2 = 'RIP 2';
var route_static = 'Enable Static Route';
var route_ipaddr = 'IP Address';
var route_mask = 'Subnet Mask';
var route_gateway = 'Gateway';
var route_metric = 'Metric';
var route_interface = 'Interface';
var route_lan = 'LAN';
var route_wan = 'WAN Intranet';
var route_internet_wan = 'WAN Internet';
var route_showtbl = 'Show Route Table';

var route_static_tbl = 'Static Route Table';

var route_tbl_destip = 'Destination IP Address';
var route_tbl_mask = 'Netmask ';
var route_tbl_gateway = 'Gateway ';
var route_tbl_metric = 'Metric ';
var route_tbl_inter = 'Interface ';
var route_tbl_select = 'Select';

var route_deletechick_warn = 'Do you really want to delete the selected entry?';
var route_deleteall_warn = 'Do you really want to delete all entries?';
var route_deletechick = 'Delete Selected';
var route_deleteall = 'Delete All';

var route_addchick0 = 'Invalid IP address ';
var route_addchick1 = 'Invalid IP address value! ';
var route_addchick2 = 'Invalid Gateway address! ';
var route_addchick3 = 'Invalid Metric value! The range of Metric is 1 ~ 15';
var route_checkip1 = 'IP address cannot be empty! It should be filled with 4-digit numbers as xxx.xxx.xxx.xxx.';
var route_checkip2 = ' Value. It should be decimal number (0-9).';
var route_checkip3 = ' Range in 1st digit. It should be 1-223.';
var route_checkip4 = ' Range in 2nd digit. It should be 0-255.';
var route_checkip5 = ' Range in 3rd digit. It should be 0-255.';
var route_checkip6 = ' Range in 4th digit. It should be 0-255.';
var route_validnum = 'Invalid value. It should be in decimal number (0-9).';
var route_setrip = 'You can not disable NAT in PPP WAN type!';

/***********	routetbl.htm ************/
var routetbl_header = 'Routing Table ';
var routetbl_header_explain = ' This table shows  the all routing entry .';
var routetbl_refresh = 'Refresh';
var routetbl_close = ' Close ';
var routetbl_dst = 'Destination';
var routetbl_gw = 'Gateway';
var routetbl_mask = 'Genmask';
var routetbl_flag = 'Flags';
var routetbl_iface = 'Interface';
var routetbl_type = 'Type';


/***********	util_gw.js************/
var util_gw_wps_warn1 = 'The SSID had been configured by WPS. Any change of the setting? ';
var util_gw_wps_warn2 = 'AP Mode had been configured by WPS. Any change of the setting? ';
var util_gw_wps_warn3 = 'The security setting had been configured by WPS. Any change of the setting ?' ;
var util_gw_wps_warn4 = 'The WPA Enterprise Authentication cannot be supported by WPS. ';
var util_gw_wps_warn5 = 'The 802.1x Authentication cannot be supported by WPS. ';
var util_gw_wps_warn6 = 'WDS mode cannot be supported by WPS. ';
var util_gw_wps_warn7 = 'Adhoc Client mode cannot be supported by WPS. ';
var util_gw_wps_cause_disconn = 'May cause stations to be disconnected. ';
var util_gw_wps_want_to = 'Are you sure to continue with the new setting?';
var util_gw_wps_cause_disabled = 'Use this configuration will cause WPS to be disabled. ';
var util_gw_wps_ecrypt_11n = 'Invalid Encryption Mode! WPA or WPA2, Cipher suite AES should be used for 802.11n band.';
var util_gw_wps_ecrypt_basic = 'The Encryption Mode is not suitable for 802.11n band, please modify wireless encryption setting, or it will not work properly.';
var util_gw_wps_ecrypt_confirm = 'Are you sure to continue with this encryption mode for 11n band? It may not get good performance while users are using wireless network!';
var util_gw_ssid_hidden_alert = 'If turn on hiddenSSID; WPS2.0 will be disabled';
var util_gw_ssid_empty = 'SSID cannot be empty';
var util_gw_ssid_not_true = 'You entered an invalid SSID (Wi-Fi network name). SSID must be entered by the English keyboard layout, including letters and numbers!';
var util_gw_preshared_key_not_true = "Error! You have entered the wrong password.";
var util_gw_preshared_key_not_trues = "Error! You have entered the wrong password.";
var util_gw_preshared_key_length_alert =  'Pre-Shared Key value should be 64 characters.';
var util_gw_preshared_key_alert = "Invalid Pre-Shared Key value. It should be in hex number (0-9 or a-f).";
var util_gw_preshared_key_min_alert = 'Pre-Shared Key value should be set at least 8 characters.';
var util_gw_wifi_password_length='(length range is 8-63)';
var util_gw_preshared_key_max_alert = 'Pre-Shared Key value should be less than 64 characters.';
var util_gw_decimal_rang = 'It should be a decimal number between 1-65535.';
var util_gw_invalid_radius_port = 'Invalid port number of RADIUS Server! ';
var util_gw_empty_radius_port = "RADIUS Server port number cannot be empty! ";
var util_gw_invalid_radius_ip = 'Invalid RADIUS Server IP address';
var util_gw_mask_empty = 'Subnet mask cannot be empty! ';
var util_gw_ip_format = 'It should be filled with 4 digit numbers as xxx.xxx.xxx.xxx.';
var util_gw_mask_rang = '\n It should be the number of 0, 128, 192, 224, 240, 248, 252 or 254';
var util_gw_mask_invalid1 = 'Invalid Subnet mask in 1st digit.';
var util_gw_mask_invalid2 = 'Invalid Subnet mask in 2nd digit.';
var util_gw_mask_invalid3 = 'Invalid Subnet mask in 3rd digit.';
var util_gw_mask_invalid4 = 'Invalid Subnet mask in 4th digit.';
var util_gw_mask_invalid = 'Invalid Subnet mask value. ';
var util_gw_decimal_value_rang = "It should be decimal number (0-9).";
var util_gw_invalid_degw_ip = 'Invalid default Gateway address';
var util_gw_invalid_gw_ip = 'Invalid Gateway address!';
var util_gw_locat_subnet = '\nIt should be located in the same Subnet of current IP address.';
var util_gw_mac_complete = 'Input MAC address is not complete. ';
var util_gw_12hex = 'It should be 12 digits in hex.';
var util_gw_invalid_mac = 'Invalid MAC address. ';
var util_gw_hex_rang = 'It should be in hex number (0-9 or a-f).';
var util_gw_ip_empty = 'IP address cannot be empty! ';
var util_gw_invalid_value = ' Value. ';
var util_gw_should_be = 'It should be ';
var util_gw_check_ppp_rang1 = ' Range in 1st digit. ';
var util_gw_check_ppp_rang2 = ' Range in 2nd digit. ';
var util_gw_check_ppp_rang3 = ' Range in 3rd digit. ';
var util_gw_check_ppp_rang4 = ' Range in 4th digit. ';
var util_gw_check_ppp_rang5 = ' Range in 5th digit. ';
var util_gw_invalid_key_length = 'Invalid length of Key ';
var util_gw_char = ' Characters.';
var util_gw_invalid_wep_key_value = 'Invalid length of WEP Key value. ';
var util_gw_invalid_key_value = 'Invalid key value. ';
var util_gw_invalid_ip = 'Invalid IP address';
var util_gw_ipaddr_empty = 'IP address cannot be empty! It should be filled with 4 digit numbers as xxx.xxx.xxx.xxx.';
var util_gw_ipaddr_nodecimal = ' Vvalue. It should be the decimal number (0-9).';
var util_gw_ipaddr_1strange = ' Range in 1st digit. It should be 0-255.';
var util_gw_ipaddr_2ndrange = ' Range in 2nd digit. It should be 0-255.';
var util_gw_ipaddr_3rdrange = ' Range in 3rd digit. It should be 0-255.';
var util_gw_ipaddr_4thrange = ' Range in 4th digit. It should be 1-254.';

var util_gw_array0 = "(GMT-12:00)Eniwetok, Kwajalein";
var util_gw_array1 = "(GMT-11:00)Midway Island, Samoa";
var util_gw_array2 = "(GMT-10:00)Hawaii";
var util_gw_array3 = "(GMT-09:00)Alaska";
var util_gw_array4 = "(GMT-08:00)Pacific Time (US & Canada); Tijuana";
var util_gw_array5 = "(GMT-07:00)Arizona";
var util_gw_array6 = "(GMT-07:00)Mountain Time (US & Canada)";
var util_gw_array7 = "(GMT-06:00)Central Time (US & Canada)";
var util_gw_array8 = "(GMT-06:00)Mexico City, Tegucigalpa";
var util_gw_array9 = "(GMT-06:00)Saskatchewan";
var util_gw_array10 = "(GMT-05:00)Bogota, Lima, Quito";
var util_gw_array11 = "(GMT-05:00)Eastern Time (US & Canada)";
var util_gw_array12 = "(GMT-05:00)Indiana (East)";
var util_gw_array13 = "(GMT-04:00)Atlantic Time (Canada)";
var util_gw_array14 = "(GMT-04:00)Caracas, La Paz";
var util_gw_array15 = "(GMT-04:00)Santiago";
var util_gw_array16 = "(GMT-03:30)Newfoundland";
var util_gw_array17 = "(GMT-03:00)Brasilia";
var util_gw_array18 = "(GMT-03:00)Buenos Aires, Georgetown";
var util_gw_array19 = "(GMT-02:00)Mid-Atlantic";
var util_gw_array20 = "(GMT-01:00)Azores, Cape Verde Is.";
var util_gw_array21 = "(GMT)Casablanca, Monrovia";
var util_gw_array22 = "(GMT)Greenwich Mean Time Dublin, Edinburgh, Lisbon, London";
var util_gw_array23 = "(GMT+01:00)Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna";
var util_gw_array24 = "(GMT+01:00)Belgrade, Bratislava, Budapest, Ljubljana, Prague";
var util_gw_array25 = "(GMT+01:00)Barcelona, Madrid";
var util_gw_array26 = "(GMT+01:00)Brussels, Copenhagen, Madrid, Paris, Vilnius";
var util_gw_array27 = "(GMT+01:00)Paris";
var util_gw_array28 = "(GMT+01:00)Sarajevo, Skopje, Sofija, Warsaw, Zagreb";
var util_gw_array29 = "(GMT+02:00)Athens, Istanbul, Minsk";
var util_gw_array30 = "(GMT+02:00)Bucharest";
var util_gw_array31 = "(GMT+02:00)Cairo";
var util_gw_array32 = "(GMT+02:00)Harare, Pretoria";
var util_gw_array33 = "(GMT+02:00)Helsinki, Riga, Tallinn";
var util_gw_array34 = "(GMT+02:00)Jerusalem";
var util_gw_array35 = "(GMT+03:00)Baghdad, Kuwait, Riyadh";
var util_gw_array36 = "(GMT+03:00)Moscow, St. Petersburg, Volgograd";
var util_gw_array37 = "(GMT+03:00)Mairobi";
var util_gw_array38 = "(GMT+03:30)Tehran";
var util_gw_array39 = "(GMT+04:00)Abu Dhabi, Muscat";
var util_gw_array40 = "(GMT+04:00)Baku, Tbilisi";
var util_gw_array41 = "(GMT+04:30)Kabul";
var util_gw_array42 = "(GMT+05:00)Ekaterinburg";
var util_gw_array43 = "(GMT+05:00)Islamabad, Karachi, Tashkent";
var util_gw_array44 = "(GMT+05:30)Bombay, Calcutta, Madras, New Delhi";
var util_gw_array45 = "(GMT+06:00)Astana, Almaty, Dhaka";
var util_gw_array46 = "(GMT+06:00)Colombo";
var util_gw_array47 = "(GMT+07:00)Bangkok, Hanoi, Jakarta";
var util_gw_array48 = "(GMT+08:00)Beijing, Chongqing, Hong Kong, Urumqi";
var util_gw_array49 = "(GMT+08:00)Perth";
var util_gw_array50 = "(GMT+08:00)Singapore";
var util_gw_array51 = "(GMT+08:00)Taipei";
var util_gw_array52 = "(GMT+09:00)Osaka, Sapporo, Tokyo";
var util_gw_array53 = "(GMT+09:00)Seoul";
var util_gw_array54 = "(GMT+09:00)Yakutsk";
var util_gw_array55 = "(GMT+09:30)Adelaide";
var util_gw_array56 = "(GMT+09:30)Darwin";
var util_gw_array57 = "(GMT+10:00)Brisbane";
var util_gw_array58 = "(GMT+10:00)Canberra, Melbourne, Sydney";
var util_gw_array59 = "(GMT+10:00)Guam, Port Moresby";
var util_gw_array60 = "(GMT+10:00)Hobart";
var util_gw_array61 = "(GMT+10:00)Vladivostok";
var util_gw_array62 = "(GMT+11:00)Magadan, Solomon Is., New Caledonia";
var util_gw_array63 = "(GMT+12:00)Auckland, Wllington";
var util_gw_array64 = "(GMT+12:00)Fiji, Kamchatka, Marshall Is.";

var util_gw_chanauto = 'Auto';
var uyi_gw_chan_dfsauto = 'Auto(DFS)';


/***********	wlwps.htm	************/
var wlwps_header = 'WPS Settings';
var wlwps_header_explain = 'This page allows you to change the setting for WPS (Wi-Fi Protected Setup). Using this feature could let your wireless client automically syncronize its setting and connect to the Access Point in a minute without any hassle.';
var wlwps_wps_disable = 'WPS';
var wlwps_wps_save = 'Apply Changes';
var wlwps_wps_reset = 'Reset';
var wlwps_status = 'WPS Status';
var wlwps_status_conn = 'Configured';
var wlwps_status_unconn = 'Unconfigured';
var wlwps_status_reset = 'Reset to Unconfigured';
var wlwps_lockdown_state = 'Auto-lock-down state';
var wlwps_self_pinnum = 'Self-PIN Number';
var wlwps_unlockautolockdown = 'Unlock';
var wlwps_lockdown_state_locked = 'Locked';
var wlwps_lockdown_state_unlocked = 'Unlocked';
var wlwps_pbc_title = 'Push Button Configuration';
var wlwps_pbc_start_button = 'Start PBC';
var wlwps_stopwsc_title = 'STOP WSC';
var wlwps_stopwsc_button = 'Stop WSC';
var wlwps_pin_number_title = 'Client PIN Number';
var wlwps_pin_start_button = 'Start PIN';
var wlwps_keyinfo = 'Current Key Info';
var wlwps_authentication = 'Authentication';
var wlwps_authentication_open = 'Open';
var wlwps_authentication_wpa_psk = 'WPA PSK';
var wlwps_authentication_wep_share = 'WEP Shared';
var wlwps_authentication_wpa_enterprise = 'WPA Enterprise';
var wlwps_authentication_wpa2_enterprise = 'WPA2 Enterprise';
var wlwps_authentication_wpa2_psk = 'WPA2 PSK';
var wlwps_authentication_wpa2mixed_psk = 'WPA2-Mixed PSK';
var wlwps_encryption = 'Encryption';
var wlwps_encryption_none= 'None';
var wlwps_encryption_wep = 'WEP';
var wlwps_encryption_tkip = 'TKIP';
var wlwps_encryption_aes = 'AES';
var wlwps_encryption_tkip_aes = 'TKIP+AES';
var wlwps_key = 'Key';
var wlwps_pin_conn = 'PIN Configuration';
var wlwps_assign_ssid = 'Assign SSID of Registrar';
var wlsps_assign_mac = 'Assign MAC of Registrar';
var wlwps_wpa_save_pininvalid = 'Invalid PIN length! The device PIN is usually four or eight digits long.';
var wlwps_wpa_save_pinnum = 'Invalid PIN! The device PIN must be numeric digits.';
var wlwps_wpa_save_pinchecksum = 'Invalid PIN! Checksum error.';
var wlwps_pinstart_pinlen = 'Invalid Enrollee PIN length! The device PIN is usually four or eight digits long.';
var wlwps_pinstart_pinnum = 'Invalid Enrollee PIN! Enrollee PIN must be numeric digits.';
var wlwps_pinstart_pinchecksum = 'Checksum failed! Use PIN anyway? ';

var warn_msg1='WPS was disabled automatically because wireless mode setting could not be supported. ' + 'You need go to Wireless/Basic page to modify settings to enable WPS.';
var warn_msg2='WPS was disabled automatically because Radius Authentication could not be supported. ' + 'You need go to Wireless/Security page to modify settings to enable WPS.';
var warn_msg3="PIN number was generated. You have to click \'Apply Changes\' button to make change work effectively.";

var wlwps_regenerate_pin = 'Regenerate PIN & Apply';
/***********	wlsch.htm	************/
var wlan_schedule_header = 'Wireless Schedule';
var wlan_schedule_explain = 'This page allows you setup the wireless schedule rule. Please do not forget to configure system time before enable this feature.';
var wlan_schedule_enable = 'Wireless Schedule';
var wlan_schedule_everyday = 'Everyday';
var wlan_schedule_sun = 'Sun';
var wlan_schedule_mon = 'Mon';
var wlan_schedule_tue= 'Tue';
var wlan_schedule_wed = 'Wed';
var wlan_schedule_thu = 'Thu';
var wlan_schedule_fri = 'Fri';
var wlan_schedule_sat = 'Sat';
var wlan_schedule_24Hours = '24 Hours';
var wlan_schedule_from = 'From';
var wlan_schedule_to = 'To';
var wlan_schedule_save = 'Apply';
var wlan_schedule_reset = 'Reset';
var wlan_schedule_days = 'Days';
var wlan_schedule_time = 'Time';
var wlan_schedule_time_check = 'Please set the Days!';
var wlan_schedule_on='On';
var wlan_schedule_explain1='Schedule Rule';
var wlan_schedule_explain2='Error. The start time later than end time';


/***********	reboot_schedule.htm	************/
var reboot_schedule_header = 'Reboot Schedule';
var reboot_schedule_explain = 'The schedule function allows you to setup the time that the router will reboot automatically.';
var reboot_schedule_enable = 'Reboot Schedule';
var reboot_schedule_everyday = 'Everyday';
var reboot_schedule_sun = 'Sun';
var reboot_schedule_mon = 'Mon';
var reboot_schedule_tue= 'Tue';
var reboot_schedule_wed = 'Wed';
var reboot_schedule_thu = 'Thu';
var reboot_schedule_fri = 'Fri';
var reboot_schedule_sat = 'Sat';
var reboot_schedule_24Hours = '24 Hours';
var reboot_schedule_from = 'From';
var reboot_schedule_to = 'To';
var reboot_schedule_save = 'Apply';
var reboot_schedule_reset = 'Reset';
var reboot_schedule_days = 'Days';
var reboot_schedule_time = 'Time';
var reboot_schedule_time_check = 'Please set the Days!';
var reboot_schedule_on='On';
var reboot_schedule_explain1='Error! Input value should be between 0-23 in decimal.';
var reboot_schedule_explain2='Error! Input value should be between 0-59 in decimal.';

/***********	wlactrl.htm	************/
var wlactrl_onelan_header = 'Access Control';
var wlactrl_morelan_header = 'Wireless Access Control -WLAN';
var wlactrl_header_explain = 'If you choose Allowed Listed, only those clients whose wireless MAC addresses are in the access control list will be able to connect to your Access Point. When Deny Listed is selected, these wireless clients on the list will not be able to connect the Access Point.';
var wlactrl_accmode = 'Wireless Access Control Mode';
var wlactrl_accmode_diable = 'Disable';
var wlactrl_accmode_allowlist = 'Allow Listed';
var wlactrl_accmode_denylist = 'Deny Listed';
var wlactrl_macaddr = 'MAC Address';
var wlactrl_comment = 'Comment ';
var wlactrl_apply = 'Apply';
var wlactrl_reset = 'Reset';
var wlactrl_accctrl_list = 'Current Access Control List';
var wlactrl_delete_select_btn = 'Delete Selected';
var wlactrl_delete_all_btn = 'Delete All';
var wlactrl_fmwlan_macaddr = 'MAC Address';
var wlactrl_fmwlan_select = 'Select';
var wlactrl_fmwlan_comment = 'Comment';
var wlactrl_apply_explain = 'If ACL allow list turn on ; WPS2.0 will be disabled';
var wlactrl_apply_mac_short = 'Input MAC address is not complete. It should be 12 digits in hex.';
var wlactrl_apply_mac_invalid = 'Invalid MAC address. It should be in hex number (0-9 or a-f).';
var wlactrl_delete_result = 'Delete all entries will lead to all clients cannot connect to AP. Sure?';
var wlactrl_delete_select = 'Do you really want to delete the selected entry?';
var wlactrl_delete_all = 'Do you really want to delete all entries?';



/***********	wladvanced.htm	************/
var wladv_vallid_num_alert = 'Invalid value. It should be in decimal number (0-9).';
var wladv_fragment_thre_alert = 'Invalid value of Fragment Threshold. Input value should be between 256-2346 in decimal.';
var wladv_rts_thre_alert = 'Invalid value of RTS Threshold. Input value should be between 0-2347 in decimal.';
var wladv_beacon_alert = 'Invalid value of Beacon Interval. Input value should be between 20-1024 in decimal.';
var wladv_limit_alert = 'Invalid value of Limit Client. Input value should be between 3-64 in decimal.';
var wladv_header = 'Advanced Settings';
var wladv_explain = ' These settings are only for more technically advanced users who have a sufficient knowledge about wireless LAN. These settings should not be changed unless you know what effect the changes will have on your Access Point.';
var wladv_frg_thre = 'Fragment Threshold';
var wladv_rts_thre = 'RTS Threshold';
var wladv_beacon_interval = 'Beacon Interval';
var wladv_limit_client_ap = 'Limit Client AP(3-64)';
var wladv_limit_client_ap1 = 'Limit Client AP1(3-64)';
var wladv_limit_client_ap2 = 'Limit Client AP2(3-64)';
var wladv_limit_client_ap3 = 'Limit Client AP3(3-64)';
var wladv_limit_client_ap4 = 'Limit Client AP4(3-64)';
var wladv_preamble_type = 'Preamble Type';
var wladv_preamble_long = 'Long Preamble';
var wladv_preamble_short = 'Short Preamble';
var wladv_iapp = 'IAPP';
var wladv_iapp_enabled = 'On';
var wladv_iapp_disabled = 'Off';
var wladv_protection = 'Protection';
var wladv_protection_enabled = 'Enabled';
var wladv_protection_disabled = 'Disabled';
var wladv_aggregation = 'Aggregation';
var wladv_aggregation_enabled = 'Enabled';
var wladv_aggregation_disabled = 'Disabled';
var wladv_short_gi = 'Short GI';
var wladv_short_gi_enabled = 'Enabled';
var wladv_short_gi_disabled = 'Disabled';
var wladv_wlan_partition = 'WLAN Partition';
var wladv_wlan_partition_enabled = 'Enabled';
var wladv_wlan_partition_disabled = 'Disabled';
var wladv_stbc = 'STBC';
var wladv_stbc_enabled = 'Enabled';
var wladv_stbc_disabled = 'Disabled';
var wladv_coexist = '20/40MHz Coexist';
var wladv_coexist_enabled = 'Enabled';
var wladv_coexist_disabled = 'Disabled';
var wladv_tx_beamform = 'TX Beamforming';
var wladv_tx_beamform_enabled = 'Enabled';
var wladv_tx_beamform_disabled = 'Disabled';
var wladv_rf_power = 'RF Output Power';
var wladv_apply = 'Apply';
var wladv_reset = ' Reset ';


/***********	wlwdstbl.htm ************/
var wlwdstbl_header = 'WDS AP Table';
var wlwdstbl_wlan = "WLAN";
var wlwdstbl_explain = ' This table shows the MAC address, transmission, receiption packet counters and state information for each configured WDS AP.';
var wlwdstbl_mac = 'MAC Address';
var wlwdstbl_tx_pkt = 'Tx Packets';
var wlwdstbl_tx_err = 'Tx Errors';
var wlwdstbl_tx_rate = 'Tx Rate (Mbps)';
var wlwdstbl_rx_pkt = 'Rx Packets';
var wlwdstbl_refresh = 'Refresh';
var wlwdstbl_close = 'Close';
/***********	wlwdsenp.htm ************/
var wlwdsenp_hex = 'HEX';
var wlwdsenp_char = 'Characters';
var wlwdsenp_header = 'WDS Security Setup';
var wlwdsenp_wlan = ' WLAN';
var wlwdsenp_explain = 'This page allows you setup the wireless security for WDS. When enabled, you must make sure each WDS device has adopted the same encryption algorithm and Key.';
var wlwdsenp_wep_key_format = 'WEP Key Format';
var wlwdsenp_encrypt = 'Encryption';
var wlwdsenp_wep_key = 'WEP Key';
var wlwdsenp_prekey_format = 'Pre-Shared Key Format';
var wlwdsenp_prekey = 'Pre-Shared Key';
var wlwdsenp_none = 'None';
var wlwdsenp_pass = 'Passphrase';
var wlwdsenp_bits = 'Bits';
var wlwdsenp_apply = "Apply Changes";
var wlwdsenp_reset = 'Reset';


/***********	wlwds.htm	************/
var wlwds_onelan_header = 'WDS Settings';
var wlwds_morelan_header = 'WDS Settings -WLAN';
var wlwds_header_explain = 'Wireless Distribution System uses wireless media to communicate with other APs, like the Ethernet does. To do this, you must set these APs in the same channel and set MAC address of other APs which you want to communicate with in the table and then enable the WDS.';
var wlwds_enable = 'WDS';
var wlwds_mac_addr = 'MAC Address';
var wlwds_data_rate = 'Data Rate';
var wlwds_rate_auto = 'Auto';
var wlwds_comment = 'Comment';
var wlwds_apply = 'Apply';
var wlwds_reset = 'Reset';
var wlwds_set_secu = 'Set Security';
var wlwd_show_stat = 'Show Statistics';
var wlwds_wdsap_list = 'Current WDS AP List';
var wlwds_delete_select = 'Delete Selected';
var wlwds_delete_all = 'Delete All';
var wlwds_fmwlan_macaddr = 'MAC Address';
var wlwds_fmwlan_txrate = 'Tx Rate (Mbps)';
var wlwds_fmwlan_select = 'Select';
var wlwds_fmwlan_comment = 'Comment';
var wlwds_macaddr_nocomplete = 'Input MAC address is not complete. It should be 12 digits in hex.';
var wlwds_macaddr_invalid = 'Invalid MAC address. It should be in hex number (0-9 or a-f).';
var wlwds_delete_chick = 'Do you really want to delete the selected entry?';
var wlwds_delete_allchick = 'Do you really want to delete all entries?';



/***********	wlsurvey.htm	************/
var wlsurvey_onewlan_header = 'Wireless Site Survey</p>';
var wlsurvey_morewlan_header = 'Wireless Site Survey -WLAN';
var wlsurvey_header_explain = 'This page provides tool to scan the wireless network. If any Access Point or IBSS is found,you could choose to connect it manually when client mode is enabled.';
var wlsurvey_header_explain1 = '';
var wlsurvey_header_explain2 = '';
var wlsurvey_site_survey = 'Scan';
var wlsurvey_chan_next = '<input type="button" value="  Next>>" id="next" onClick="saveClickSSID()">';
var wlsurvey_encryption = 'Encryption';
var wlsurvey_encryption_no = 'NONE';
var wlsurvey_encryption_wep = 'WEP';
var wlsurvey_encryption_wpa = 'WPA';
var wlsurvey_encryption_wpa2= 'WPA2';

var wlsurvey_keytype = 'Key Type';
var wlsurvey_keytype_open = 'Open';
var wlsurvey_keytype_shared = 'Shared';
var wlsurvey_keytype_both = 'Both';

var wlsurvey_keylen = 'Key Length';
var wlsurvey_keylen_64 = '64-bit';
var wlsurvey_keylen_128 = '128-bit';

var wlsurvey_keyfmt = 'Key Format';
var wlsurvey_keyfmt_ascii = 'ASCII';
var wlsurvey_keyfmt_hex = 'Hex';

var wlsurvey_keyset = 'Key Setting';

var wlsurvey_authmode = 'Authentication Mode';
var wlsurvey_authmode_enter_radius = 'Enterprise (RADIUS)';
var wlsurvey_authmode_enter_server = 'Enterprise (AS Server)';
var wlsurvey_authmode_personal = 'Personal (Pre-Shared Key)';

var wlsurvey_wpacip = 'WPA Cipher Suite';
var wlsurvey_wpacip_tkip = 'TKIP';
var wlsurvey_wpacip_aes = 'AES';
var wlsurvey_wp2chip = 'WPA2 Cipher Suite';
var wlsurvey_wp2chip_tkip = 'TKIP';
var wlsurvey_wp2chip_aes = 'AES';

var wlsurvey_preshared_keyfmt = 'Pre-Shared Key Format';
var wlsurvey_preshared_keyfmt_passphrase = 'Passphrase';
var wlsurvey_preshared_keyfmt_hex = 'HEX (64 characters)';
var wlsurvey_preshared_key = 'Pre-Shared Key';

var wlsurvey_eaptype = 'EAP Type';
var wlsurvey_eaptype_md5 = 'MD5';
var wlsurvey_eaptype_tls = 'TLS';
var wlsurvey_eaptype_peap = 'PEAP';

var wlsurvey_intunn_type = 'Inside Tunnel Type';
var wlsurvey_intunn_type_MSCHAPV2 = 'MSCHAPV2';

var wlsurvey_eap_userid = 'EAP User ID';
var wlsurvey_radius_passwd = 'RADIUS User Password';
var wlsurvey_radius_name = 'RADIUS User Name';
var wlsurvey_user_keypasswd = 'User Key Password (if any)';


var wlsurvey_use_as_server = 'Use Local AS Server';
var wlsurvey_as_ser_ipaddr = 'AS Server IP Address';
var wlsurvey_back_button = '<<Back  ';
var wlsurvey_conn_button = 'Connect';
var wlsurvey_wait_explain = 'Please wait...';
var wlsurvey_inside_nosupport = 'This inside type is not supported.';
var wlsurvey_eap_nosupport = 'This EAP type is not supported.';
var wlsurvey_wrong_method = 'Wrong method!';
var wlsurvey_nosupport_method = 'Error not supported method ID';
var wlsurvey_nosupport_wpa2 = 'Error not supported WPA2 cipher suite ';
var wlsurvey_nosupport_wpasuit = 'Error not supported WPA cipher suite ';
var wlsurvey_nosupport_encry = 'Error not supported encryption ';

var wlsurvey_tbl_ssid = 'SSID';
var wlsurvey_tbl_bssid = 'BSSID';
var wlsurvey_tbl_chan = 'Channel';
var wlsurvey_tbl_type = 'Type';
var wlsurvey_tbl_ency = 'Encrypt';
var wlsurvey_tbl_signal = 'Signal';
var wlsurvey_tbl_select = 'Select';
var wlsurvey_tbl_macaddr = 'MAC Address';
var wlsurvey_tbl_meshid = 'Mesh ID';
var wlsurvey_tbl_none = 'None';

var wlsurvey_read_site_error = 'Read Site-Survey status failed!';
var wlsurvey_get_modemib_error = 'Get MIB_WLAN_MODE MIB failed!';
var wlsurvey_get_bssinfo_error = 'Get bssinfo failed!';



/***********	wlsecutity.htm wlsecutity_all.htm	************/
var wlsec_validate_note = "Note if you choose [Enterprise (AS Server)] and apply changes here, \nthis wlan interface and its virtual interfaces will use the current AS setting.\nDo you want to continue?";
var wlsec_header = ' Wireless Security Setup';
var wlsec_explain = '  This page allows you setup the wireless security. Turn on WEP or WPA by using Encryption Keys could prevent any unauthorized access to your wireless network.';
var wlsec_select_ssid = 'Select SSID';
var wlsec_psk= 'PSK';
var wlsec_pre_shared = 'Pre-Shared Key';
var wlsec_tkip = 'TKIP';
var wlsec_aes = 'AES';
var wlsec_apply = 'Apply';
var wlsec_reset = 'Reset';
var wlsec_inside_type_alert = "This inside type is not supported.";
var wlsec_eap_alert = 'This EAP type is not supported.';
var wlsec_wapi_remote_ca_install_alert = "Please make sure that WAPI certs from remote AS have already been installed at web page [WAPI] -> [Certificate Install].";
var wlsec_wapi_local_ca_install_alert = "Please make sure that WAPI certs from local AS have already been installed at web page [WAPI] -> [Certificate Install].";
var wlsec_wapi_wrong_select_alert = "Wrong WAPI cert selected index.";
var wlsec_wapi_key_length_alert = "The WAPI key should be at least 8 characters and no more than 32 characters";
var wlsec_wapi_key_hex_alert = "The Hex Mode WAPI key length should be 64 hex number";
var wlsec_wapi_invalid_key_alert = "Invalid key value. It should be in hex number (0-9 or a-f).";
var wlsec_encryption_none ="Encryption is none now";
var wlsec_802_1x_none = '802.1x EAP inside type is not supported here!';
var wlsec_802_1x_not_support='802.1x EAP type is not supported here!';
var wlsec_wep_confirm = "If WEP is turn on,WPS2.0 will be disabled";
var wlsec_wpa_confirm = "Under WPA only or TKIP only, WPS2 daemon will be disabled";
var wlsec_wpa2_empty_alert = "WPA2 Cipher Suite can't be empty.";
var wlsec_wpa_empty_alert = "WPA Cipher Suite can't be empty.";
var wlsec_tkip_confirm = "If TKIP only, WPS2.0 will be disabled";
var wlsec_encryption =  'Encryption';
var wlsec_disabled = 'Disabled';
var wlsec_wpa_mix = 'WPA-Mixed';
var wlsec_802_1x = '802.1x Authentication';
var wlsec_auth = 'Authentication';
var wlsec_auth_open_sys = 'Open System';
var wlsec_auth_shared_key = 'Shared Key';
var wlsec_auth_auto = 'Auto';
var wlsec_key_length = 'Key Length';
var wlsec_key_hex = 'HEX';
var wlsec_key_ascii = 'ASCII';
var wlsec_encryption_key = 'Encryption Key';
var wlsec_auth_mode = 'Authentication Mode';
var wlsec_auth_enterprise_mode = 'Enterprise (RADIUS)';
var wlsec_auth_enterprise_ap_mode = 'Enterprise (AS Server)';
var wlsec_auth_personal_mode = 'Personal (Pre-Shared Key)';
var wlsec_wpa_suite = 'WPA Cipher Suite';
var wlsec_wpa2_suite = 'WPA2 Cipher Suite';
var wlsec_wep_key_format = 'Key Format';
var wlsec_pre_key_format = 'Pre-Shared Key Format';
var wlsec_pre_key = 'Pre-Shared Key';
var wlsec_passpharse = 'Passphrase';
var wlsec_key_hex64 = 'HEX (64 characters)';
var wlsec_key_64bit = '64 Bits';
var wlsec_key_128bit = '128 Bits';
var wlsec_radius_server_ip = "RADIUS&nbsp;Server&nbsp;IP&nbsp;Address";
var wlsec_radius_server_port = 'RADIUS&nbsp;Server&nbsp;Port';
var wlsec_radius_server_password = 'RADIUS&nbsp;Server&nbsp;Password';
var wlsec_eap_type = 'EAP Type';
var wlsec_inside_tunnel_type = 'Inside Tunnel Type';
var wlsec_eap_user_id = 'EAP User ID';
var wlsec_radius_user = 'RADIUS User Name';
var wlsec_radius_user_password = 'RADIUS User Password';
var wlsec_user_key_password = 'User Key Password (if any)';
var wlsec_use_local_as = 'Use Local AS Server';
var wlsec_as_ip = 'AS&nbsp;Server&nbsp;IP&nbsp;Address';
var wlsec_select_wapi_ca = 'Select WAPI Certificate';
var wlsec_use_ca_from_as = 'Use Cert from Remote AS';
var wlsec_wlan0 = 'WLAN 5 G';
var wlsec_wlan1 = 'WLAN 2.4 G';

var wlsec_wlan00 = ' 5 G';
var wlsec_wlan11 = ' 2.4 G';
var wlsec_repeater_ssid = 'Repeater SSID';
var wlsec_repeater_change_ssid = 'Change SSID and Password';
var wlsec_repeater_network_name= 'Network Name(SSID)';
var wlsec_repeater_password = 'Password';
var wlsec_unlimit='0:Unlimit';

/************************wlmultipleap.htm*****************/
var wlmultiple_header='Multiple SSIDs';
var wlmultiple_explain=' This page shows and updates the wireless setting for multiple APs.';
var wlmultiple_number='No.';
var wlmultiple_enable='Enable';
var wlmultiple_band='Band';
var wlmultiple_ssid='SSID';
var wlmultiple_data_rate='Data Rate';
var wlmultiple_broadcast_ssid='Broadcast SSID';
var wlmultiple_wmm='WMM';
var wlmultiple_access='Access';
var wlmultiple_active_client_list='Active Client List';
var wlmultiple_disabled='Disabled';
var wlmultiple_enabled='Enabled';
var wlmultiple_apply='Apply';
var wlmultiple_show='Show';
var wlmultiple_ap1='AP1';
var wlmultiple_ap2='AP2';
var wlmultiple_ap3='AP3';
var wlmultiple_ap4='AP4';
/**********************end******************/




/***********	wlbasic.htm	************/
var wlbasic_header=' Wireless Basic Setting';
var wlbasic_5ghz_header=' Wireless Basic Setting-WLAN 5G';
var wlbasic_2ghz_header=' Wireless Basic Setting-WLAN 2.4G';
var wlbasic_explain = 'This page is used to configure the parameters for wireless LAN clients which may connect to your Access Point. Here you may change wireless encryption settings as well as wireless network parameters.';
var wlbasic_network_type = 'Network Type';
var wlbasic_ssid = 'SSID';
var wlbasic_disabled = 'Radio';
var wlbasic_country = 'Country';
var wlbasic_country_region = 'Country Region';
var wlbasic_band= 'Band';
var wlbasic_bandb = '2.4 GHz (B)';
var wlbasic_bandg = '2.4 GHz (G)';
var wlbasic_bandn2 = '2.4 GHz (N)';
var wlbasic_bandbg = '2.4 GHz (B+G)';
var wlbasic_bandgn = '2.4 GHz (G+N)';
var wlbasic_bandbgn = '2.4 GHz (B+G+N)';
var wlbasic_banda = '5 GHz (A)';
var wlbasic_bandn ='5 GHz (N)';
var wlbasic_bandan = '5 GHz (A+N)';
var wlbasic_bandabgn = '2.4GHz + 5 GHz (A+B+G+N)';
var wlbasic_bandanac = '5 GHz (A+N+AC)';
var wlbasic_bandac = '5 GHz (AC)';
var wlbasic_bandnac = '5 GHz (N+AC)';
var wlbasic_bandaac = '5 GHz (A+AC)';
var wlbasic_wlan_phyband ='5Ð“Ð“Ñ†';
var wlbasic_infrastructure = "Infrastructure";
var wlbasic_adhoc = "Adhoc";
var wlbasic_addprofile = 'Add to Profile';
var wlbasic_channelwidth = 'Channel Width';
var wlbasic_channelwidth0 = '20MHz';
var wlbasic_channelwidth1 = '40MHz';
var wlbasic_channelwidth2 = '80MHz';
var wlbasic_ctlsideband = 'Control Sideband';
var wlbasic_ctlsideautomode = 'Auto';
var wlbasic_ctlsidelowermode = 'Lower';
var wlbasic_ctlsideuppermode = 'Upper';
var wlbasic_chnnelnum = 'Channel Number';
var wlbasic_broadcastssid= 'Broadcast SSID';
var wlbasic_brossid_enabled = 'Enabled';
var wlbasic_brossid_disabled = 'Disabled';
var wlbasic_wmm ='WMM';
var wlbasic_wmm_disabled = 	'Disabled';
var wlbasic_wmm_enabled = 	'Enabled';
var wlbasic_data_rate = 'Data Rate';
var wlbasic_data_rate_auto = "Auto";
var wlbasic_associated_clients = 'Associated Clients';
var wlbasic_show_associated_clients = 'Show Active Clients';
var wlbasic_enable_mac_clone = 'Enable MAC Clone (Single Ethernet Client)';
var wlbasic_enable_repeater_mode = 'Enable Universal Repeater Mode (Acting as AP and client simultaneously)';
var wlbasic_extended_ssid = 'SSID of Extended Interface';
var wlbasic_ssid_note = 'Note if you want to change the setting for Mode and SSID, you must go to EasyConfig page to disable EasyConfig first.';
var wlbasic_enable_wl_profile = 'Enable Wireless Profile';
var wlbasic_wl_profile_list = 'Wireless Profile List';
var wlbasic_apply = 'Apply';
var wlbasic_reset = 'Reset';
var wlbasic_delete_select = 'Delete Selected';
var wlbasic_delete_all = 'Delete All';
var wlbasic_enable_wire = 'Do you also want to enable wireless profile?';
var wlbasic_asloenable_wire = 'Do you also want to enable wireless profile?';

var wlbasic_mode = 'Mode';
var wlbasic_client = 'Client';
var wlbasic_gotosurvey ='Find Wi-Fi networks';
var wlbasic_interface_header = 'Wireless Setting';
var wlbasic_region = 'Region:';

/*************aliasip.htm*********************************/
var aliasip_header = 'Alias IP Setup';
var aliasip_explain = 'From this page you can register aliases to IP ports Access Point.';
var aliasip_enable = 'Enable Alias IP';
var aliasip_table = 'Alias IP Table';

/**********************nat_addr_map.htm************/
var nataddrmap_header = 'NAT Mapping';
var nataddrmap_enable = 'Enable NAT Address Mapping';
var nataddrmap_publicip = 'Public IP Address';
var nataddrmap_privateip = 'Private IP Address';
var nataddrmap_comment = 'Comment';
var nataddrmap_table = 'Current NAT Address Mapping Table';
var nataddrmap_invalid_publicip = 'Invalid Public IP address';
var nataddrmap_invalid_privateip = 'Invalid Private IP address';


/***********	tcpipwan.htm  tcpiplan.htm************/
var tcpip_check_ip_msg = 'Invalid IP address';
var tcpip_check_server_ip_msg = 'Invalid Server IP address';
var tcpip_check_dns_ip_msg1 = 'Invalid DNS1 address';
var tcpip_check_dns_ip_msg11 = 'DNS1 address cannot be empty!';
var tcpip_check_dns_ip_msg2 = 'Invalid DNS2 address';
var tcpip_check_dns_ip_msg3 = 'Invalid DNS3 address';
var tcpip_check_dns_ip_msg4 = 'Invalid DNS4 address';
var tcpip_check_dns_ip_msg5 = 'Invalid DNS5 address';
var tcpip_check_dns_ip_msg6 = 'Invalid DNS6 address';
var tcpip_check_size_msg = "Invalid MTU size! You should set a value between 1400-1500. ";
var tcpip_check_size_msg1 = "Invalid MTU size! You should set a value between 1400-1492. ";
var tcpip_check_size_msg2 = "Invalid MTU size! You should set a value between 1360-1492. ";
var tcpip_check_size_msg3 = "Invalid MTU size! You should set a value between 1400-1460. ";

var tcpip_check_user_name_msg = "User name cannot be empty!";
var tcpip_check_password_msg = "Password cannot be empty!";
var tcpip_check_invalid_time_msg = "Invalid idle time value! You should set a value between 1-1000. ";
var tcpip_pppoecontype_alert = "Wrong PPPoE ConnType";
var tcpip_pptpontype_alert = "Wrong PPTP ConnType";
var tcpip_l2tpcontype_alert = "Wrong L2TP ConnType";
var tcpip_pppcontype_alert = "Wrong PPP ConnType";
var tcpip_browser_alert = 'Error! Your browser must have CSS support !';
var tcpip_wan_header = 'WAN Settings';
var tcpip_wan_explain = '  This page is used to configure the parameters for Internet network which connects to the WAN port of your Access Point. Here you may change the access method to static IP, DHCP, PPPoE, PPTP or L2TP  by click the item value of WAN Access type.';
var tcpip_wan_pppoe_connection = 'PPPoE Connection';
var tcpip_wan_auto_dns = 'Attain DNS Automatically';
var tcpip_wan_manually_dns =  'Set DNS Manually';
var tcpip_wan_conn_time = '&nbsp;(1-1000 minutes)';
var tcpip_wan_max_mtu_size = 'Bytes';
var tcpip_wan_conn = 'Connect';
var tcpip_wan_disconn = 'Disconnect';
var tcpip_wan_continuous = 'Continuous';
var tcpip_wan_on_demand = 'Connect on Demand';
var tcpip_wan_manual = 'Manual';
var tcpip_wan_ieee21x_auth_mode = 'Authentication method';
var tcpip_wan_ieee21x_auth_disable = 'Disabled';
var tcpip_wan_ieee21x_auth_md5 = 'EAP-MD5';
var tcpip_wan_ieee21x_auth_ttls = 'EAP/TTLS-MD5';
var tcpip_wan_ieee21x_auth_user = 'Login Name';
var tcpip_wan_ieee21x_auth_pass = 'Password'; 
var tcpip_wan_dhcp_route = 'DHCP Route';
var tcpip_wan_access_type = 'WAN Type';
var tcpip_wan_type_static_ip = 'Static IP';
var tcpip_wan_type_client = "DHCP Client";
var tcpip_wan_type_pppoe = "PPPoE/Dual WAN Access PPPoE";
var tcpip_wan_type_pptp = "PPTP/Dual WAN Access PPTP";
var tcpip_wan_type_l2tp = "L2TP/Dual WAN Access L2TP"; 
var tcpip_wan_type_usb3g = "USB3G/Dual WAN Access USB3G";	
//var tcpip_wan_ip = "IP Address";
//var tcpip_wan_mask = 'Subnet Mask';
//var tcpip_wan_default_gateway = 'Default Gateway';
var tcpip_wan_mtu_size = 'MTU Size';
var tcpip_wan_host = 'Host Name';
var tcpip_wan_user = 'User Name';
var tcpip_wan_password = 'Password';
var tcpip_wan_server_ac = 'Service Name';
var tcpip_wan_ac = 'AC Name';
var tcpip_wan_conn_type = 'Connection Type';
var tcpip_wan_idle_time = 'Idle Time';
var tcpip_wan_server_ip = 'Server IP/Domain Name Address';
var tcpip_wan_clone_mac = 'Clone MAC Address';
var tcpip_wan_clone_mac_scan = 'Scan MAC Address';
var tcpip_wan_web_server_port = 'Web Server Port';
var tcpip_wan_ssh_server_port = 'SSH Server Port';
var tcpip_wan_enable_upnp = '&nbsp;&nbsp;Enable uPNP';
var tcpip_wan_disable_ttl = '&nbsp;&nbsp;Disable TTL-1';
var tcpip_wan_enable_igmp_proxy = '&nbsp;&nbsp;Enable IGMP Proxy';
var tcpip_wan_enable_igmp_snooping = '&nbsp;&nbsp;Enable IGMP Snooping';
var tcpip_wan_enable_ping = '&nbsp;&nbsp;Enable Ping Access on WAN';
var tcpip_wan_enable_webserver = '&nbsp;&nbsp;Enable Web Server Access on WAN';
var tcpip_wan_enable_ftpserver = '&nbsp;&nbsp;Enable FTP Server Access on WAN';
var tcpip_wan_enable_ipsec = '&nbsp;&nbsp;Enable IPsec pass through on VPN connection';
var tcpip_wan_enable_pptp = '&nbsp;&nbsp;Enable PPTP pass through on VPN connection';
var tcpip_wan_enable_l2tp = '&nbsp;&nbsp;Enable L2TP pass through on VPN connection';
var tcpip_wan_enable_ipv6 = '&nbsp;&nbsp;Enable IPv6 pass through on VPN connection';
var tcpip_wan_enable_netsniper='Enable NetSniper';
var tcpip_apply = 'Apply';
var tcpip_reset = 'Reset';
var tcpip_lan_wrong_dhcp_field = "Wrong DHCP field!";
var tcpip_lan_start_ip = 'Invalid DHCP client start address!';
var tcpip_lan_end_ip = 'Invalid DHCP client end address!';
var tcpip_lan_ip_alert = '\nIt should be located in the same Subnet of current IP address.';
var tcpip_lan_invalid_rang = 'Invalid DHCP client address range!\nEnding address should be greater than starting address.';
var tcpip_lan_invalid_rangs = 'Invalid DHCP client address range!';
var tcpip_lan_invalid_rang_value = "Invalid value. It should be in range (1 ~ 10080).";
var tcpip_lan_invalid_dhcp_type = "Load invalid DHCP type!";
var tcpip_lan_header = 'LAN Settings';
var tcpip_lan_explain = ' This page is used to configure the parameters for local area network which connects to the LAN port of your Access Point. Here you may change the setting for IP addresss, subnet mask, DHCP, etc..';
var tcpip_lan_mac = "MAC Address";
var tcpip_lan_ip = "IP Address";
var tcpip_change_lanip ="Auto change";
var tcpip_lan_mask = 'Subnet Mask';
var tcpip_lan_default_gateway = 'Default Gateway';
var tcpip_lan_dns_lan_side='LAN-side router IP';
var tcpip_lan_dns_wan_side='Actual WAN DNS';
var tcpip_lan_dns_manual='DNS Manually';
var tcpip_lan_dhcp = 'DHCP';
var tcpip_lan_dhcp_disabled = 'Disabled';
var tcpip_lan_dhcp_client = 'Client';
var tcpip_lan_dhcp_server = 'Server';
var tcpip_lan_dhcp_auto = 'Auto';
var tcpip_lan_dhcp_rang = 'DHCP Client Range';
var tcpip_lan_dhcp_time = 'DHCP Lease Time';
var tcpip_minutes = 'Minutes';
var tcpip_lan_staicdhcp = 'Static DHCP';
var tcpip_staticdhcp = "Set Static DHCP";
var tcpip_domain = "Domain Name";
var tcpip_802_1d = "802.1d Spanning Tree";
var tcpip_802_1d_enable = 'Enabled';
var tcpip_802_1d_disabled = 'Disabled';
var tcpip_show_client = 'DHCP&ARP Tables';
var tcpip_gw_invalid_gw_ip = 'Invalid Gateway address!';
var tcpip_2nd_header='For IP Routing';
var tcpip_2nd_enable = 'Enabled';
var tcpip_2nd_disable = 'Disabled';
var tcpip_2nd_ip = 'Second IP Address';
var tcpip_2nd_mask = 'Second Subnet Mask';
var menu_tcpip_2nd = 'For IP Routing';


var tcpip_wan_invalid_gw_ip = 'Invalid Gateway address';
var tcpip_wan_phy_dhcp_type='WAN DHCP Type';
var tcpip_wan_phy_org_pppoe='Normal PPPoE';
var tcpip_wan_pppoe_constant='Constant';
var tcpip_wan_pppoe_auto='Connection on demand';
var tcpip_wan_pppoe_manually='Connection Manually';
var tcpip_wan_need_mppe='Enable MPPE Encryption';
var tcpip_wan_need_mppc='Enable MPPC compression';
var tcpip_dns_type='DNS Type';
/**********************************************/

/***********	3g4g.htm ************/
var menu_usb3g='3G/4G Settings';
var usb3g_wan_header = '3G/4G Settings';  
var usb3g_Location = 'Location';  
var usb3g_Mobile_ISP = 'Mobile ISP';
var usb3g_pin = 'PIN';
var usb3g_apn = 'APN';
var usb3g_dial_number = 'Dial Number';
var usb3g_mtu = 'MTU';
var usb3g_wan_explain = '  This page is used to configure the parameters for Internet network which connects to the WAN port of your Access Point. Here you may change the access method to USB3G by click the item value of WAN Access type.';


 
/**********************************************/
var wan_access_header = 'Internet Access';  
var wan_access_explain = 'The router provides some Internet access modes for you to choose';
var wan_access_mode1 = '3G/4G Only (Recommended) --Only use 3G/4G as the access to the Internet. (RJ45 Ethernet Port as LAN)';
var wan_access_mode2 ='3G/4G Preferred --Use 3G/4G as the primary access, WAN as a backup. (RJ45 Ethernet Port as WAN)';
var wan_access_mode3 ='WAN Preferred --Use WAN as the primary access, 3G/4G as a backup.  (RJ45 Ethernet Port as WAN)';
var wan_access_mode4 ='WAN Only --Only use WAN as the access to the Internet.  (RJ45 Ethernet Port as WAN)';
/**********ConnModeCfgRpm.htm******************/

/***********	tcpip_staticdhcp.htm ************/
var tcpip_dhcp_del_select = 'Do you really want to delete the selected entry?';
var tcpip_dhcp_del_all = 'Do you really want to delete all entries?';
var tcpip_dhcp_header = 'Static DHCP Setup';
var tcpip_dhcp_explain = 'This page allows you reserve IP addresses, and assign the same IP address to the network device with the specified MAC address any time it requests an IP address. This is almost the same as when a device has a static IP address except that the device must still request an IP address from the DHCP server. ';
var tcpip_dhcp_explain1 = '';
var tcpip_dhcp_explain2 = '';
var tcpip_dhcp_explain3 = '';
var tcpip_dhcp_explain4 = '';
var tcpip_dhcp_explain5 = '';
var tcpip_dhcp_explain6 = '';
var tcpip_dhcp_st_enabled = 'Static DHCP';
var tcpip_dhcp_comment = 'Comment';
var tcpip_dhcp_list = 'Static DHCP List';
//var tcpip_dhcp_apply = "Apply Changes";
//var tcpip_dhcp_reset = 'Reset';
var tcpip_dhcp_delsel = 'Delete Selected';
var tcpip_dhcp_delall = 'Delete All';
var tcpip_dhcp_select = 'Select';

/***********	dhcptbl.htm ************/
var dhcp_header = 'DHCP Clients & ARP Table';
var dhcp_explain = ' This table shows the assigned IP address, MAC address and time expired for each DHCP leased client.';
var dhcp_client_table = 'DHCP Clients Table';
var dhcp_arp_table = 'ARP Table';
var dhcp_ip = 'IP Address';
var dhcp_mac = 'MAC Address';
var dhcp_time = 'Remaining Lease Time (in seconds)';
var dhcp_hostname = 'Host Name';


/***********	wizard.htm ************/
var wizard_header = 'Setup Wizard';
var wizard_header_explain = 'The setup wizard will guide  you to configure access point for first time. Please follow the setup wizard step by step.';
//var wizard_welcome = 'Welcome to Setup Wizard.';
//var wizard_content_explain = 'The Wizard will guide you the through following steps. Click Next to start.';
var wizard_mode_content ='Setup Operation Mode';
var wizard_mode_explain = ' This page shows you the selected router mode. ';
var wizard_content1 = 'Show Operation Mode';
var wizard_content2 = 'Choose your Time Zone';
var wizard_content3 = 'Setup LAN Interface';
var wizard_content4 = 'Setup WAN Interface';
var wizard_content5 = 'Select Wireless Band';
var wizard_content6 = 'Wireless Basic Setting';
var wizard_content7 = 'Wireless Security Setting';
var wizard_content8 = 'Wireless Basic Setting 2.4G';
var wizard_content9 = 'Wireless Security Setting 2.4G';
var wizard_content10 = 'IPTV Settings';
var wizard_next_btn = '  Next>>';
var wizard_back_btn = '<<Back  ';
var wizard_cancel_btn = '  Cancel  ';
var wizard_finish_btn = 'Connect';

var wizard_opmode_invalid = 'Invalid Operation Mode value ';
var wizard_chanset_wrong = 'Wrong field input!';
var wizard_wantypeselect = 'Error! Your browser must have CSS support !';
var wizard_weplen_error = 'Invalid MIB value length0';

var wizard_content5_explain = 'You can select Wireless Band.';
var wizard_wire_band = 'Wireless Band';

var wizard_basic_header_explain = ' This page is used to configure the parameters for wireless LAN clients which may connect to your Access Point. ';
var wizard_wlan1_div0_mode = 'Mode';
var wizard_chan_auto = 'Auto';
var wizard_client = 'Client';

var wizard_wpa_tkip = 'WPA (TKIP)';
var wizard_wpa2_aes = 'WPA2(AES)';
var wizard_wpa2_mixed = 'WPA2 Mixed';
var wizard_use_cert_from_remote_as0 = ' Use Cert from Remote AS0 ';
var wizard_use_cert_from_remote_as1 = ' Use Cert from Remote AS1 ';

var wizard_5G_basic = 'Wireless 5GHz Basic Settings';
var wizard_5G_sec = 'Wireless 5G Security Setup';
var wizard_2G_basic = 'Wireless 2.4GHz Basic Settings';
var wizard_2G_sec = 'Wireless 2.4G Security Setup';


/***********	ntp.htm	************/
var ntp_header = 'Time Zone Setting';
var ntp_header_explain = 'You can maintain the system time by synchronizing with a public time server over the Internet.';
var ntp_curr_time = ' Current Time  ';
var ntp_year = 'Yr ';
var ntp_month = 'Mon ';
var ntp_day = 'Day ';
var ntp_hour = 'Hr ';
var ntp_minute = 'Mn ';
var ntp_second = 'Sec ';
var ntp_copy_comptime = 'Copy Computer Time';
var ntp_time_zone = 'Time Zone Select  ';
var ntp_enable_clientup = 'Enable NTP client update ';
var ntp_adjust_daylight = 'Automatically Adjust Daylight Saving ';
var ntp_server = ' SNTP server  ';
//var ntp_server_north_america1 = '198.123.30.132    - North America';
//var ntp_server_north_america2 = '209.249.181.22   - North America';
//var ntp_server_Europe1 = '85.12.35.12  - Europe';
//var ntp_server_Europe2 = '217.144.143.91   - Europe';
//var ntp_server_Australia = '223.27.18.137  - Australia';
//var ntp_server_asia1 = '133.100.11.8 - Asia Pacific';
//var ntp_server_asia2 = '210.72.145.44 - Asia Pacific';
var ntp_server_americal=' - North America';
var ntp_server_Europe = ' - Europe';
var ntp_server_Australia = ' - Australia';
var ntp_server_asia = ' - Asia Pacific';
var ntp_manu_ipset = ' (Manual IP Setting) ';
//var ntp_apply = 'Apply Change';
//var ntp_reset = 'Reset';
//var ntp_refresh = 'Refresh';
var ntp_month_invalid = 'Invalid month number. It should be decimal (1-9).';
var ntp_time_invalid = 'ÐÐµÐºÐ¾Ñ€Ñ€ÐµÐºÑ‚Ð½Ð¾Ðµ Ð²Ñ€ÐµÐ¼Ñ!';
var ntp_ip_invalid = 'Invalid IP address';
var ntp_servip_invalid = 'Invalid NTP Server IP address! It can not be empty.';
//var ntp_field_check = ' Field can't be empty\n';
var ntp_invalid = 'Invalid ';
var ntp_num_check = ' Number. It should be decimal (0-9).';



/***********	syslog.htm	************/
var syslog_header = 'System Log';
var syslog_header_explain = 'This page can be used to set remote log server and show the system log.';
var syslog_enable = 'Log';
var syslog_sys_enable = ' System all ';
var syslog_wireless_enable = 'Wireless';
//var syslog_dos_enable = 'DoS';
//var syslog_11s_enable = '11s';
var syslog_rtlog_enable = 'Enable Remote Log';
var syslog_local_ipaddr = 'Log Server IP Address';
var syslog_clear = ' Clear ';
var syslog_ip_invalid = 'Invalid IP address';


/***********	stats.htm ************/
var stats_header = 'Statistics';
var stats_explain = ' This page shows the packet counters for transmission and reception regarding to wireless and Ethernet networks.';
var stats_lan = ' LAN';
var stats_send = 'Sent Packets';
var stats_recv = 'Received Packets';
var stats_repeater = ' Repeater ';
var stats_eth = 'Ethernet';
var stats_wan = ' WAN';



/***********	ddns.htm	************/
var ddns_header = 'DDNS Settings';
var ddns_header_explain = 'Dynamic DNS is a service, that provides you with a valid, unchanging, internet domain name (an URL) to go with that (possibly everchanging) IP-address';
var ddns_enable = 'DDNS';
var ddns_serv_provider = ' Service Provider';
var ddns_domain_name = 'Domain Name';
var ddns_user_name = ' User Name/Email';
var ddns_passwd = ' Password/Key';
var ddns_domain_name_empty = 'Domain Name can\'t be empty';
var ddns_user_name_empty = 'User Name/Email can\'t be empty';
var ddns_passwd_empty = 'Password/Key can\'t be empty';
var ddns_time= 'Min';
var ddns_status='DDNS Status';
var ddns_domain_ip='Domain IP';
var ddns_status_success='DDNS Update Successfully';
var ddns_status_false='Authentication Faiedl';


/***********	opmode.htm	************/
var opmode_explain='';
var opmode_help='Operation Mode Help';
var opmode_apply_next='Apply/Next>>';
var opmode_radio_gw='Router Mode';
var opmode_radio_gw_explain='In this mode, the device is supposed to connect to the internet via ADSL/Cable modem. The WAN type can be setup on WAN page, including PPPOE, DHCP Client, PPTP Client, L2TP Client and Static IP.';
var opmode_radio_br='Bridge Mode';
var opmode_radio_br_explain='In this mode, the device is used as an Access Point and all ports  become LAN ports. It will access the Internet by wired connection to upper router. All the WAN related functions and firewall are not supported.';
var opmode_radio_wisp='Wireless Client';
var opmode_radio_wisp_explain='In this mode, the router only allows users to access the Internet by wired connection. You can connect to the ISP AP on Site-Survey page.';
var opmode_radio_wisp_wanif='WAN Interface  ';
var opmode_radio_br_repeater=' Repeater(Extender)';
var opmode_radio_br_repeater_explain='This mode extends your existing wireless network to wider coverage. You can access the the Internet by wireless or wired connection to the device';
var opmode_radio_br_ap='AP Mode';
var opmode_radio_br_ap_explain='Combine two local networks via wireless connection. You can only connect to the device by cable.';
var opmode_radio_br_client='Client';
var opmode_radio_br_client_explain='The router is used as a "Wireless Adapter" to connect your wired devices(e.g.Xbox/PS3) to a wireless network.';
var opmode_radio_wisp_ap='WISP Mode';
var opmode_radio_wisp_ap_explain='In this mode, the device connect to WISP Station wirelessly through PPPOE/DHCP Clien/PPTP Client/L2TP Client/Static IP WAN types. You are able to share Internet via local wireless and wired network';

/************opmode_help.htm*********************/
var opmode_help_menu='Operation Mode Help';
var opmode_help_client_router='Wireless ISP Client';
var opmode_help_client_router_explain='In this mode, the device enables multiple users to access the Internet from WISP. All connected terminals share the same IP through wireless connection.';
var opmode_help_router='Router';
var opmode_help_router_explain='In this mode,the device enables multiple users to share Internet via ADSL/Cable Modem.The wireless connection share the same IP to ISP through Ethernet WAN port.';
var opmode_help_bridge_with_ap='Bridge with AP';
var opmode_help_bridge_with_ap_explain='In this mode, the device can be used to combine multiple local networks together via wireless connections, designed for home or offices where separated networks can not be connected easily using cable';
var opmode_help_repeater='Repeater(Range Extender)';
var opmode_help_repeater_explain='In this mode, the device can copy and reinforce the existing wireless signal to extend the wireless coverage, designed for a large space to eliminate hard-to-reach areas';
var opmode_help_client='Client';
var opmode_help_client_explain='In this mode, the device can be connected to another device via Ethernet port and act as an adaptor to allow your wired devices to access a wireless network, designed for Smart TU, Mdia Player, or game console only with an Ethernet port';
var opmode_help_be_sure='Be sure to click the Save button to save your settings on this page';
//var opmode_help_note1='Note';
//var opmode_help_note1_explain='The router will reboot automatically after you click the Save button';
//var opmode_help_note2_explain='When you change the operation mode to Client/Repeater, WPS function will be disabled. Please manually enable this function if needed when you get back to Access Point/Multi-SSID/Bridge mode.';


/***********	menu.htm	************/

var menu_rootap="Basic Settings";
var menu_vap1="Multiple SSID 1";
var menu_vap2="Multiple SSID 2";
var menu_vap3="Multiple SSID 3";
var menu_vap4="Multiple SSID 4";
var menu_client="Wireless Repeater";

var menu_ipv6="IPv6 Network";
var menu_tr069="TR-069 Config";
var menu_reboot="Reboot";
var menu_title='Wireless 11n Router';
var menu_status='Status';
var menu_system_status='Status';
var menu_statistics='Statistics';
var menu_log='System Log';

var menu_tcpip='Network';
var menu_lan='LAN Settings';
var menu_wan='WAN Settings';  
var menu_aliasip='Alias IP';
var menu_nataddrmap='NAT Mapping';
var menu_vlan='IPTV Settings';
var menu_timeZone='Time Zone Settings';

var menu_wireless='Wireless';
var menu_wireless0='Wireless 5GHz';
var menu_wireless1='Wireless 2.4GHz';

var menu_basic='Basic Settings';
var menu_security='Security Settings';
var menu_siteSurvey='Site Survey';
var menu_wds='WDS Settings';
var menu_advance='Advanced Settings';
var menu_accessControl='Access Control';
var menu_wps='WPS Settings';
var menu_schedule='Wireless Schedule';
var menu_reboot_schedule='Reboot Schedule';

var menu_routeSetup='Route Setup';
var menu_staticRoute='Static Route';
var menu_routeTbl = 'Routing Table';

var menu_fireWall='Firewall';
var menu_portFilter='Port Filtering';
var menu_ipFilter='IP/Port Filtering';
var menu_spiFwall='SPI Firewall';
var menu_algSip='SIP ALG';
var menu_macFilter='MAC Filtering';
var menu_portFw='Port Forwarding';
var menu_urlFilter='URL Filtering';
var menu_dmz='DMZ';
var menu_dos='Denial-of-Service';

var menu_management='System';
var menu_updateFm='Upgrade Firmware';
var menu_settings='Save/Reload Settings';
var menu_pwd='Password Settings';
var menu_adv='Advanced';
var menu_ddns='DDNS Settings';
var menu_qos='QoS';
var menu_opmode ='Operation Mode';
var menu_sysCmd ='Run Command';
var menu_sshServer ='SSH Server';

var menu_wizard ='Setup Wizard';
var menu_quicksetup ="Easy Setup";
var menu_snmp = 'SNMP Setup';
var menu_refreshbuffer = 'Refresh Buffer Memory';


var menu_remoteManage = "Remote Management"
var menu_phone_status='Status';
var menu_phone_ea_setup='Easy Setup';
var menu_phone_es_as='Advanced Setup';
var menu_phone_reboot = "Reboot";
/**************** end menu.htm *******************/


/************** ssh.htm *******************/
var ssh_page_info='SSH Server, allow you login router by command GUI.';
var ssh_enabled='Enable SSH';
var ssh_user='SSH User';
var ssh_passwd='SSH Password';

/**************** status.htm *******************/
var status_explain = "This page shows the current status and some basic settings of the device.";
var status_need_reboot = "Please reboot the router for changes to take effect";
var status_need_setlanip = "IPÂ addressÂ conflict!Â PleaseÂ changeÂ theÂ IPÂ addressÂ inÂ â€œLANÂ Settingsâ€";
var status_lan_config = "LAN Configuration";
var status_sys = 'System';
var status_wan_config = "WAN Configuration";
var status_connect_type = 'Connection type';
var status_wifi_connect_type = 'Connection type';
var status_ip = 'IP Address';
//var status_subnet_mask = 'Subnet Mask';
//var status_default_gw = 'Default Gateway';
var status_mac = 'MAC Address';
var status_lan_client_num = 'Connected Clients';
var status_dhcp_server = 'DHCP Server';
var status_disabled = 'Disabled';
var status_enabled = 'Enabled';
var status_auto = 'Auto';
var status_reboot = "Reboot Router";
var status_system = 'System';
var status_uptime = 'Uptime';
var status_fw_ver = 'Firmware Version';
var status_build_time = 'Build Time';
var status_max_sessions = 'Max sessions';
var status_curr_sessions ='Current sessions';
var status_wifi = 'Wi-Fi';
var status_config = 'Configuration';
var status_wifi_config = 'Wi-Fi Configuration';
var status_wifi_5ghz = '1';
var status_wifi_2ghz = '2';
var status_secondary_connection = 'Secondary Connection';
var status_secondary_ip = 'IP Address';
var status_secondary_subnet_mask = 'Subnet Mask';
var status_secondary_connect_type = 'WAN DHCP Type';

var status_wifi_mode = "Mode";
var status_client_mode_inf = "Infrastructure Client";
var status_client_mode_adhoc = "Ad-hoc Client";
var status_ap = 'AP';
var status_wds = 'WDS';
var status_ap_wds = 'AP+WDS';
var status_mesh = 'MESH';
var status_ap_mesh = 'AP+MESH';
var status_mpp = 'MPP';
var status_ap_mpp = 'AP+MPP';
var status_map = 'MAP';
var status_mp = 'MP';
var status_band = 'Band';
var status_signal = 'Signal Strength';

var status_ch_num = 'Channel Number';

var status_encrypt = 'Encryption';
var status_vap  = 'Virtual AP';
var status_repeater = "WISP Status";
var status_mode = "Mode";
var status_connected_clients = 'Connected Clients';
var status_wl = 'Wireless';
var status_5ghz = '5 GHz';
var status_2ghz = '2.4 GHz';
var status_ipv6_lan_config = 'LAN IPv6 Configuration';
var status_ipv6_global_address = 'Global Address';
var status_ipv6_ll_address = 'LL Address';
var status_ipv6_wan_config = 'WAN IPv6 Configuration';
var status_ipv6_link_type = 'Link Type';
var status_ipv6_connect_type = 'Connection Type';

var selet_enable = 'Enabled';
var selet_disable = 'Disabled';

var select_dis='Disable';
var select_enb='Enable';
var on_off='';

var wlsecur_bs='Basic Settings';
var wlsecur_ma1='Multiple SSID 1';
var wlsecur_ma2='Multiple SSID 2';
var wlsecur_ma3='Multiple SSID 3';
var wlsecur_ma4='Multiple SSID 4';
var wlsecur_wr='Wireless Repeater';

var dhcp_en='DHCP';
var en_acecon='Access Control ';
var wlsec_need_enable_repeater = 'WISP mode requires Wireless Repeater Client enabled as WAN!';

var lg_ul = 'USER LOGIN';
var lg_ts = 'The server ';
var lg_requp = ' requires a username and password.';
var lg_un = 'User Name';
var lg_pass = 'Password';

var es_discn='Disconnected';
var es_connec='Connected';
var es_as='Advanced Setup';
var ea_setup='Easy Setup';
var es_exp='The easy setup will guide you to configure AP for first time.';
var es_cs='Connect Status';
var es_cs_tag='Connect Status';		
var es_is='Internet Setting';
var es_ws='Wireless Setting';
var qs_sub='Apply';
var qs_res='Refresh';

var es_wsh_5G='5G Wireless Setting';
var es_wsh='Wireless Setting';

var es_wsl='2.4G Wireless Setting';
var es_ssid='SSID';

var cs_sc='Successful Connection !!';
var cs_suctell='You have successful entered all information required to setup your router';
var cs_back='Back';
var cs_finish='Finish';
var cs_cf='Connection Failed!!';
var cs_try='Please try again later';
var cs_try_btn='Try again';

var cdp_sr='System is rebooting';
var cdp_ss='Setting successfully!';
var cdp_pw='Please wait ';
var cdp_sec='Seconds...';

var JS_ip_check_8 = "Invalid IP address";
var JS_mac_check_1 = "Input MAC address is not complete. It should be 12 digits in hex.";
var JS_mac_check_2 = "Invalid MAC address, It should be in hex number (0-9 or a-f).";
var JS_mac_check_4 = "Invalid MAC address. It should not be 00:00:00:00:00:00 or ff:ff:ff:ff:ff:ff.";
var JS_mac_check_5 = "Invalid MAC address. It should not be multicast MAC address between 01:00:5e:00:00:00 and 01:00:5e:7f:ff:ff.";

var scan_head ="Current Wireless IP/MAC List";
var scan_explain ="This page shows the wireless current IP/MAC table.";
var scan_refresh ="Refresh";
var scan_close ="Close";
var scan_site ="Site Survey";
var scan_site_explain ="Site survey page shows information of APs nearby. You may choose one of these APs connecting or adding it to profile.";

var Add_New_Rule = "Add a rule(Max number of rule is 10)";
var Add_New_Rule_4 = "Add a rule(Max number of rule is 4)";
var Add_New_Rule_20 = "Add a rule(Max number of rule is 20)";
var Add_New_Rule_32 = "Add a rule(Max number of rule is 32)";
var JS_max_entry_2 = "The maximum entry count is ";

var ddns_upt_interval = "Auto-Update Interval";
var wlwds_site_server = "Survey Site List";

var vlan_enb = "IPTV";

var title2_help = "Help";
var title2_reboot = "Reboot";

var menu_notice = "Notice Setting";
var notice_onoff = "Notice";
var notice_whitelist_url_onoff = "White list URL On/Off"
var notice_whitelist_sip_onoff = "White list source IP On/Off"
var notice_pic = "LOGO:";
var notice_newpic = "Upload new LOGO ( png format, size < 4080 Bytes)";
var notice_title = "Title";
var notice_content = "Content";
var notice_notice_info = "Click the button to continue access the Internet.";
var notice_preview = " Preview ";
var notice_url_invalid = "Invalid URL ... include ';'"
var notice_ip4_invalid = "Invalid IP address, it should be in 1~254 !"
var notice_sip_range_invalid = "Invalid source IP range, start IP bigger than end IP.";
var notice_ip4_invalid = "Invalid IP address, it should be in 1~254 !"
var notice_timeout = "Notice Timeout Value";
var notice_timeout_unit = "(1 ~ 1440 minutes)";
var notice_timeout_invalid = "Invalid notice timeout value, the unit is minute, and ."
var notice_select_disabled='Disabled';
var notice_select_enabled='Enabled - notice URL with single IP';
var notice_select_enabled_ips='Enabled';
var notice_notice_url='Input your notice URL';
var notice_btn_name='User phrases button';

////////////////////usbshare.htm/////////////////////////////////////////
var menu_usbshare='USB Share';
var usbshare_header='USB Application (Share Server) Setting';
var usbshare_dlna_enabled='Enable DLNA';
var usbshare_samba_enabled='Enable SAMBA';
var usbshare_ftp_enabled='Enable FTP';

////////////logout.htm/////////////////////////////////
var menu_logout='Logout';
var logout_is='Do you want to logout ?';
var logout_apply = 'Apply';



/////////////////////////////////////////////////////////////////////////

var menu_snmp = "SNMP";

var menu_ipv6Basic = "Basic Settings for IPv6";
var menu_dhcp6s = "DHCP Server for IPv6";
var menu_radvd = "RADVD for IPv6";
var menu_tunnel6 = "IPv6 Tunnel to IPv4";
var page_scan ="Scan";

function dw(str)
{
	document.write(str);
}
