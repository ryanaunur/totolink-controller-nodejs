function _camelize(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (letter, index) {
        return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
    }).replace(/\s+/g, '');
}

function parserPort(data) {
    return data.map((port, index) => {
        if (port.includes('Port_WAN')) {
            return {
                type: 'wlan',
                port: (5 - index),
                status: true
            }
        } else if (port.includes('Port_LAN')) {
            return {
                type: 'lan',
                port: (5 - index),
                status: true
            }
        } else {
            return {
                type: 'lan',
                port: (5 - index),
                status: false
            }
        }
    })
}

function parseConfiguration(data) {
    const _key = function(column) {
        column = column.toLowerCase()

        return (!column.includes(' ')) ? column : _camelize(column)
    }

    return Object.assign({}, ...data.column.map((n, index) => ({
        [(_key(n))]: data.value[index]
    })))
}
function parseSystem(data) {
    const parsed = parseConfiguration(data)

    const device = {
        cpu: parsed.cpuLoad.replace('%', '') * 1,
        sdram: parsed.sdramUtilization.replace('%', '') * 1
    }

    delete parsed.cpuLoad
    delete parsed.sdramUtilization

    return Object.assign({ device }, parsed)
}
function parseLAN(data) {
    const parsed = parseConfiguration(data)

    const address = parsed.ipAddress.split(' / ')

    const ip = {
        address: address[0],
        subnetMask: address[1],
        gateway: address[2]
    }

    delete parsed.ipAddress

    return Object.assign({ ip }, parsed)
}

module.exports = {
    parserPort,
    parseLAN,
    parseSystem,
    parseConfiguration
}