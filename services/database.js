const Datastore = require('nedb-promises')
const uuid = require('uuid/v4')

class Database {
    constructor() {
        this._db = Datastore.create('../db/db.db')
    }

    gets() {
        return this._db.find({ _table: this.table }).sort({ _onInsert: -1 })
    }
    push(data) {

        return this._db.insert(Object.assign({ _table: this.table }, data))
    }
}

class Devices extends Database {
    constructor() {
        super()

        this.table = 'devices'
    }

    getAll() {
        return this.gets()
    }

    insert(data) {
        return this.push({
            alias: data.alias,
            ip: data.ip,
            ssid: data.ssid,
            status: "alive",
            _onInsert: Date.now()
        })
    }
}

module.exports = {
    devices: new Devices()
}