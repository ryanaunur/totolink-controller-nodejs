const axios = require('axios')
const puppeteer = require('puppeteer')
const elements = require('./elements')
const _ = require('underscore')
const uuid = require('uuid/v4')
const minify = require('html-minifier').minify
const async = require('async')

const path = require('path')
const fs = require('fs')

const state = {
    isReady: false
}

async function _initPuppeteer() {
    const browser = await puppeteer.launch()
    const page = await browser.newPage()

    state.page = page
    state.isReady = true

    return page
}

function _delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time)
    });
}
function _toInteger(value) {
    const ifValueNumber = value * 1

    return (_.isNumber(ifValueNumber) && !_.isNaN(ifValueNumber) && value != "") ? ifValueNumber : value
}
function _serialTask(tasks) {
    return tasks.reduce((promiseChain, currentTask) => {
        return promiseChain.then(chainResults =>
            currentTask.then(currentResult =>
                [ ...chainResults, currentResult ]
            )
        ).catch(console.error)
    }, Promise.resolve([]))
}
function _clearTemp(webIds) {
    console.info(`Clear cache [${webIds.length} files]`)
    // remove file
    webIds.forEach(id => {
        fs.unlinkSync(path.join(__dirname, `../temp/${id}.htm`))
    })
}
async function _parseContent(url, options) {
    const page = options.page

    await page.goto(`file:///${path.join(__dirname, `../temp/${options.webId}.htm`)}`, {
        waitUntil: 'load'
    });

    const selectorElements = {
        scripts: '.divcss5 table script',
        ports: 'table.port_wth tr td img',
        wifiConfiguration: {
            column: '.divcss5 table:nth-child(3) td.itemstus_td',
            values: '.divcss5 table:nth-child(3) td:nth-child(2)'
        },
        wifiClientConnected: {
            column: '.divcss5 table:nth-child(4) td.itemstus_td',
            values: '.divcss5 table:nth-child(4) td:nth-child(2)'
        },
        lanConfiguration: {
            column: '.divcss5 table:nth-child(6) td.itemstus_td',
            values: '.divcss5 table:nth-child(6) td:nth-child(2)'
        },
        system: {
            column: '.divcss5 table:nth-child(8) td.itemstus_td',
            values: '.divcss5 table:nth-child(8) td:nth-child(2)',
        }
    }

    const evaluateElement = await page.evaluate(selectorElements => {
        function _getElement(element) {
            return Array.from(document.querySelectorAll(element))
        }

        const elements = document.querySelectorAll(selectorElements.scripts)

        for (var i = 0; i < elements.length; i++) {
            elements[i].parentNode.removeChild(elements[i])
        }

        const _ports = _getElement(selectorElements.ports).map(port => port.src)
        const _wifiConf = {
            column: _getElement(selectorElements.wifiConfiguration.column).map(item => item.textContent.trim()),
            values: _getElement(selectorElements.wifiConfiguration.values).map(item => item.textContent.trim())
        }
        const _wifiConnect = {
            column: _getElement(selectorElements.wifiClientConnected.column).map(item => item.textContent.trim()),
            values: _getElement(selectorElements.wifiClientConnected.values).map(item => item.textContent.trim())
        }
        const _lanConf = {
            column: _getElement(selectorElements.lanConfiguration.column).map(item => item.textContent.trim()),
            values: _getElement(selectorElements.lanConfiguration.values).map(item => item.textContent.trim())
        }
        const _system = {
            column: _getElement(selectorElements.system.column).map(item => item.textContent.trim()),
            values: _getElement(selectorElements.system.values).map(item => item.textContent.trim())
        }
        return {
            _ports,
            _wifiConf,
            _wifiConnect,
            _lanConf,
            _system
        }
    }, selectorElements)

    const ports = evaluateElement._ports
    const wifiConfiguration = {
        column: evaluateElement._wifiConf.column,
        value: evaluateElement._wifiConf.values.map(_toInteger)
    }
    const wifiClientConnected = {
        column: evaluateElement._wifiConnect.column,
        value: evaluateElement._wifiConnect.values.map(_toInteger)
    }
    const lanConfiguration = {
        column: evaluateElement._lanConf.column,
        value: evaluateElement._lanConf.values.map(_toInteger)
    }
    const systemDetails = {
        column: evaluateElement._system.column,
        value: evaluateElement._system.values.map(_toInteger)
    }

    console.timeEnd('getStatus - ' + options.ip)

    await page.goto('about:blank')
    
    const wifiConfigurationParsed = elements.parseConfiguration(wifiConfiguration)
    const wifiClientConnectedParsed = elements.parseConfiguration(wifiClientConnected)
    const lanConfigurationParsed = elements.parseLAN(lanConfiguration)
    const systemDetailParsed = elements.parseSystem(systemDetails)

    return {
        ports: elements.parserPort(ports),
        wifi: Object.assign(wifiClientConnectedParsed, wifiConfigurationParsed),
        lan: lanConfigurationParsed,
        system: systemDetailParsed
    }
}

async function _fetchStatus(ip, options) {
    console.time('getStatus - ' + ip)

    const url = `http://${ip}/status.htm`
    const res = await axios.get(url, { timeout: options.timeout })
        .then(res => Object.assign({
            success: true
        }, res))
        .catch(error => Object.assign({
            success: false
        }, error))

    if (!res.success) return {
        success: false,
        code: "ERR_UNKOWN_DEVICE"
    }

    const data = res.data
    const contentType = res.headers['content-type']

    if (contentType !== "text/html") return {
        success: false,
        code: "ERR_CONTENT_TYPE_MISSMATCH"
    }

    const webId = uuid()
    const html = data
        .replace('util_gw.js', `file:///${path.join(__dirname, '../cache/n300rt/util_gw.js')}`)
        .replace('jquery.min.js', `file:///${path.join(__dirname, '../cache/n300rt/jquery.min.js')}`)
        .replace('language_en.js', `file:///${path.join(__dirname, '../cache/n300rt/language_en.js')}`)

    fs.writeFileSync(path.join(__dirname, `../temp/${webId}.htm`), minify(html, {
        minifyJS: true,
        removeComments: true,
        collapseWhitespace: true
    }))

    return _parseContent(url, Object.assign({
        ip,
        webId
    }, options))
}

async function getStatus(ips, options) {
    console.time('Fetch Status Hanler Running')

    if (!_.isArray(ips)) {
        ips = [ips]
    }
    const page = (!state.isReady) ? await _initPuppeteer() : state.page

    ips = (!_.isArray(ips)) ? [ips] : ips
    options = Object.assign({ timeout: 1000, page }, options)
    
    const operationResults = []
    const ids = []

    return new Promise((resolve, reject) => {
        async.eachSeries(ips, (ip, next) => {
            _fetchStatus(ip, options).then(res => {
                operationResults.push(Object.assign({ _v: true }, res))
                ids.push(res.webId)
    
                next(null)
            }).catch(error => {
                operationResults.push({ _v: false, error: error.toString(), ip })
            }) 
        }, error => {
            const result = (operationResults.length == 1) ? operationResults[0] : operationResults
            
            resolve(result)

            // clear html cache on temp folder
            _clearTemp(ids)
        })
    })    
}


module.exports = {
    getStatus,
    _initPuppeteer
}