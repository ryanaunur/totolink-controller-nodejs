const cidr = require('cidr-range')
const axios = require('axios')
const _ = require('underscore')
const isReachable = require('is-reachable')

function _getRange(subnet) {
    return cidr(subnet)
}
function _testAddress(address, timeout, _time) {
    const _start = Date.now()

    isReachable('sindresorhus.com')
        .then(reachable => {
            return axios.get(`http://${address}/wan_status.htm`, { timeout })
        })
        .then(res => Object.assign({
            success: true
        }, res))
        .catch(error => Object.assign({
            success: false
        }, error))
        .then(res => {
            _time(address, Date.now() - _start, _start)

            if (!res.status) return false

            const contentType = res.headers['content-type']
            if (contentType !== "text/html") return false

            return { address }
        })
}
function _timeCallback(address, time, _start) {
    console.log(address, time, _start)
}

async function scan(subnet, timeout = 1000) {
    const networks = (!_.isArray(subnet)) ? Array(subnet) : subnet 

    const availableAddress = networks.map(network => _getRange(network))
    const parsing = _.chunk(_.flatten(availableAddress), 10)

    const multipleOperations = parsing.map(items => Promise.all(items.map(item => _testAddress(item, timeout, _timeCallback))))
    const multipleOperationResults = await Promise.all(multipleOperations)

    return _.flatten(multipleOperationResults.map(items => items.filter(item => item)))
}

module.exports = {
    scan
}