const express = require('express')
const helmet = require('helmet')
const _ = require('underscore')

const parser = require('./services/parser')
const aliases = require('./aliases.json')

const app = express()

app.use(helmet())

parser._initPuppeteer().then(function() {
    console.info("Browser ready!")
})

app.get('/get/:ip', (req, res) => {
    console.time('Request Hanler Running')

    const target = (!_.isUndefined(aliases[req.params.ip])) ? aliases[req.params.ip] : req.params.ip

    parser.getStatus(target).then(result => {
        console.timeEnd('Request Hanler Running')

        return res.json(result)
    })
})

app.use('/app', express.static('client'))

app.listen(3000, () => {
    console.info("Listening on port :3000")
})

// Socket IO
const io = require('socket.io')()
const database = require('./services/database')


io.on('connection', client => {
    console.log(`client connected`)

    client.on('event', console.log)
    client.on('get', data => {
        parser.getStatus(data.ip).then(result => {
    
            client.emit("get", { req: data, data: result })
        })
        
    })
    client.on('getDevices', function() {
        console.log('received: getDevices')

        database.devices.getAll().then(devices => {
            client.emit('devices', devices)
        })
    })
})

io.listen(3001)